// Story:  A user is going to update one of his tasks

// In order to: update a task
// As a: recognised user
// I want to: change the % complete or finish date from the View Tasks page

// Scenario 1: I'm not registered (tasks list)
// Given: An unrecognised user has opened the application
// And: At least one task is open in the Task List (main) page
// And: the Task detail is shown (Work Items visible)
// Then: the % Complete and End Date drop-down lists are disabled

// Scenario 2: I'm registered (tasks list)
// Given: A recognised user has opened the application
// And: At least one task is open in the Task List (main) page
// And: the Task detail is shown (Work Items visible)
// Then: the % Complete and End Date drop-down lists are enabled

// Scenario 3: I make a % complete change (tasks list)
// Given: A recognised user has opened the application
// And: At least one task is open in the Task List (main) page
// And: the Task detail mode is selected (Work Items visible)
// And: the % Complete drop-down lists are enabled
// When: I select a value from the list that's different to the current value
// Then: the new % complete is shown for that work item
// And: the new % complete value is updated for that work item's time bar

// Scenario 4: I make a "end date" change (tasks list)
// Given: A recognised user has opened the application
// And: At least one task is open in the Task List (main) page
// And: the Task detail mode is selected (Work Items visible)
// And: the "End Date" drop-down lists are enabled
// When: I select that it should slip by one month
// Then: the new end date is shown for that work item
// And: the new end date is shown for that work item's time bar
