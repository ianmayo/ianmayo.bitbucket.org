// Story:  A user is going to add a status report

// In order to: add a status report
// As a: recognised user
// I want to: enter and submit a new status report for a task

// Scenario 1.1: I'm not registered (tasks list)
// Given: An unrecognised user has opened the application
// And: At least one task is open in the Task List (main) page
// When: the tasks list is shown
// Then: the "Add Status Report" button isn't shown

// Scenario 1.2: I am registered (tasks list)
// Given: A recognised user has opened the application
// And: At least one task is open in the Task List (main) page
// When: the tasks list is shown
// Then: the "Add Status Report" button is shown

// Scenario 1.3: I'm adding a status report (tasks list)
// Given: A recognised user has opened the application
// And: At least one task is open in the Task List (main) page
// When: I click on "Add Status Report"
// Then: the "Status Report" modal window opens
// And: the window includes the task name
// And: the "Status" drop-down shows the current task status
// And: the "Comment" box is empty
// And: the "Change status" button is disabled, indicating that a comment is necessary

// Scenario 1.4: I need to add a status report (tasks list)
// Given: A recognised user has opened the application
// And: At least one task is open in the Task List (main) page
// And: Both "Ongoing" and "Pending" filters are visible
// And: I've clicked on "Add Status Report"
// And: the "Status Report" modal window opens
// And: I've entered some text into the "Comment" box
// And: I've changed the status to "ongoing"/"pending" (a value that's different to the existing value)
// And: the "Change status" button is enabled
// When: I press the "Change Status" Button
// Then: The modal dialog is closed
// And: the status for the relevant task is updated
// And: the new comment is shown against that task

// Scenario 1.5: I decide not to add a status report (tasks list)
// Given: A recognised user has opened the application
// And: At least one task is open in the Task List (main) page
// And: Both "Ongoing" and "Pending" filters are visible
// And: I've clicked on "Add Status Report"
// And: the "Status Report" modal window opens
// And: I've entered some text into the "Comment" box
// And: I've changed the status to "ongoing"/"pending" (a value that's different to the existing value)
// And: the "Change status" button is enabled
// When: I press the "Cancel" button
// Then: The modal dialog is closed
// And: the status for the relevant task is unchanged
// And: the original comment is shown against that task

// Scenario 1.6: I change the status of a task so its no longer visible (tasks list)
// Given: A recognised user has opened the application
// And: Just the "Pending" filters are visible
// And: At least one task is open in the Task List (main) page
// And: I've clicked on "Add Status Report"
// And: the "Status Report" modal window opens
// And: I've entered some text into the "Comment" box
// And: I've changed the status to "ongoing"
// And: the "Change status" button is enabled
// When: I press the "Change Status" Button
// Then: The modal dialog is closed
// And: the task has disappeared



// Scenario 2.1: I'm not registered (view task page)
// Given: An unrecognised user has opened the application
// And: At least one task is open in the Task List (main) page
// And: The user has drilled down into its View Task page
// When: the View Task page is shown
// Then: the "Add Status Report" button isn't shown

// Scenario 2.2: I am registered  (view task page)
// Given: A recognised user has opened the application
// And: At least one task is open in the Task List (main) page
// When: The user has drilled down into its View Task page
// Then: the "Add Status Report" button is shown

// Scenario 2.3: I'm adding a status report  (view task page)
// Given: A recognised user has opened the application
// And: At least one task is open in the Task List (main) page
// And: I've navigated to the "View Task" page for a task
// When: I click on "Add Status Report"
// Then: the "Status Report" modal window opens
// And: the window includes the task name
// And: the "Status" drop-down shows the current task status
// And: the "Comment" box is empty
// And: the "Change status" button is disabled, indicating that a comment is necessary

// Scenario 2.4: I need to add a status report (view task page)
// Given: A recognised user has opened the application
// And: At least one task is open in the Task List (main) page
// And: Both "Ongoing" and "Pending" filters are visible
// And: I've navigated to the "View Task" page for a task
// And: I've clicked on "Add Status Report"
// And: the "Status Report" modal window opens
// And: I've entered some text into the "Comment" box
// And: I've changed the status to "ongoing"/"pending" (a value that's different to the existing value)
// And: the "Change status" button is enabled
// When: I press the "Change Status" Button
// Then: The modal dialog is closed
// And: the new status is shown in the status lozenge
// And: the new comment is shown at the top of the comments list

// Scenario 2.5: I decide not to add a status report  (view task page)
// Given: A recognised user has opened the application
// And: At least one task is open in the Task List (main) page
// And: Both "Ongoing" and "Pending" filters are visible
// And: I've navigated to the "View Task" page for a task
// And: I've clicked on "Add Status Report"
// And: the "Status Report" modal window opens
// And: I've entered some text into the "Comment" box
// And: I've changed the status to "ongoing"/"pending" (a value that's different to the existing value)
// And: the "Change status" button is enabled
// When: I press the "Cancel" button
// Then: The modal dialog is closed
// And: the status lozenge for this task is unchanged
// And: the original comment is shown against that task


