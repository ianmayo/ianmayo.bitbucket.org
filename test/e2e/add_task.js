// Story:  A user is going to add a task

// In order to: add a task
// As a: recognised user
// I want to: enter and submit the task details

// Scenario 1: a deliverable must be present
// Given: A user has opened the application
// And: he has clicked on "add task"
// When: he clicks on the "Submit" button
// Then: a warning indicates that he can't submit until at least one work item has been specified

// Scenario 2: essential fields must be present
// Given: A user has opened the application
// And: he has clicked on "add task"
// And: he has specified at least one work item
// When: he clicks on the "Submit" button
// Then: a warning indicates that he can't submit until all essential fields are provided

// Scenario 3: Pending tasks are shown
// Given: A user has opened the application
// And: just the "Pending" filter is set
// And: no pending tasks are present
// And: he has clicked on "add task"
// And: he has specified at least one work item
// And: he has provided essential fields
// When: he clicks on the "Submit" button
// Then: he returns to the home page, and one pending task is shown