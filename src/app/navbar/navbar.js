/**
 * @module Navbar
 * The NavBar is created as a "module without routes", or "global module" - so it sits in the global
 * app module scope. As such it can include directives & services.
 */

angular.module('taskApp.navbar', [
    'placeholders',
    'ui.bootstrap',
    'taskApp.addTask'
])

/**
 * @module Navbar
 * @class NavbarCtrl (controller)
 */
.controller('NavbarCtrl', function NavbarController ($scope) {
    // do nothing for the time being
});
