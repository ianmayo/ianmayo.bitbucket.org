/**
 * This is the main e2e "prototypical" scenario of the App which can help you to understand angular e2e principles
 * It is created just for learning and will be changed in the future.
 */

describe('Task Tracker main flow', function () {
    var url = '/base/build/index.html';

    var stubTask = {
        title: 'Test',
        name: 'Test',
        tally: 'Test',
        phone: '007007007',
        description: 'Test',
        impact: 'Test',
        resource: 'Test',
        priority: 'High',
        leader: 'Hod_24',
        division: 'Admin',
        organisation: 'ACME Ltd',
        categories: ['urgent'],
        workItemName: 'Item'
    };

    var addTask = function () {
        // click on the "add" button on the main page
        element('.addTask').click();

        // check URL partial: /add
        expect(browser().window().hash()).toEqual('/add');

        // populate inputs and selects
        input('defaultTask.title').enter(stubTask.title);
        input('defaultTask.customer_name').enter(stubTask.name);
        input('defaultTask.customer_tally').enter(stubTask.tally);
        input('defaultTask.customer_phone').enter(stubTask.phone);
        input('defaultTask.description').enter(stubTask.description);
        input('defaultTask.impact').enter(stubTask.impact);
        input('defaultTask.resource').enter(stubTask.resource);

        // Just use "using" when you try to populate input within ng-repeat
        using('.workItems tbody tr:nth-of-type(1)').input('workItem.name').enter(stubTask.workItemName);

        select('defaultTask.priority').option(stubTask.priority);
        select('defaultTask.division').option(stubTask.division);
        select('defaultTask.customer_organisation').option(stubTask.organisation);
        select('defaultTask.leader').option(stubTask.leader);
        select('defaultTask.categories').option(stubTask.categories);

        // wait on unblocking of submit button a bit
        sleep(1);

        // click "add task" button
        element('input[type="submit"]:first').click();
    };

    beforeEach(function () {
        browser().navigateTo(url);
    });

    describe('Initial', function () {

        it('check if we are on the main page', function () {

            // check rendered HTML: Top heading is correct, and ng-view content has correct heading
            expect(element('.brand', 'top heading').text()).toContain('Task Tracker');

            // check URL partial: /list
            expect(browser().window().hash()).toEqual('/list');
        });
    });

    describe('Add task module', function () {

        it('tries to add new task and check if it exist on the "main" page', function () {

            // create new task for testing
            addTask();

            // check URL partial: /list
            expect(browser().window().hash()).toEqual('/list');

            // check if this task exists on the main page
            expect(element('.tasks-list .task-item:last .task-title').text(), 'tasks list').toContain(stubTask.title);
        });
    });

    describe('View task module', function () {

        it('creates new task and check if it has correct title and description on the "view task" page', function () {

            // create new task for testing
            addTask();

            // click on the last element in the tasks list
            element('.tasks-list .task-item:last .task-title').click();

            // check URL partial: /add
            expect(browser().window().hash()).toEqual('/task/20');

            // check the current task's title
            expect(element('.title-container h1').text()).toMatch(stubTask.title);

            // check the current task's description
            expect(element('.description').text()).toMatch(stubTask.description);

            // let's go to the home page
            element('.brand').click();

            // check URL partial: /list
            expect(browser().window().hash()).toEqual('/welcome');
        });
    });
});
