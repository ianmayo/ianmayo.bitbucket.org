/**
 * @module FocusOnElement directive
 */
angular.module('focusOnElementDirective', [])

.directive('focusOnElement', function () {
    return function(scope, elem) {
        var focusClass = 'isFocused';

        elem[0].focus();

        // add the class to mark an element as focused in a custom way
        elem.addClass(focusClass);

        // remove the class when an element loses focus
        elem.bind('blur', function () {
            elem.removeClass(focusClass);
        });
    };
});
