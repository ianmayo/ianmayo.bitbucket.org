/**
 * modalStatusReport directive module's unit test spec
 */

describe('FocusOnElementDirective section', function () {
    var scope, elm, $compile;

    beforeEach(module('focusOnElementDirective'));

    beforeEach(inject(function (_$rootScope_, _$compile_) {
        $compile = _$compile_;
        scope = _$rootScope_.$new();

        elm = angular.element(
            '<div class="test"></div>');

        $compile(elm)(scope);

        scope.$digest();
    }));

    describe('Set focus on an element', function () {
        it('should have focused element', function() {
            var focusClass = 'isFocused';

            // add children elements
            elm.html(
                '<input type="text">' +
                '<input type="text" id="focused" data-focus-on-element>' +
                '<input type="text">');
            $compile(elm)(scope);

            expect(elm.find('input[type="text"]').length).toEqual(3);

            // the second elements should be focused
            expect(elm.find('input:eq(1)').hasClass(focusClass)).toBeTruthy();

            // remove focus from the element
            elm.find('input:eq(1)').blur();

            // the second elements should not be focused
            expect(elm.find('input:eq(1)').hasClass(focusClass)).toBeFalsy();
        });
    });

});