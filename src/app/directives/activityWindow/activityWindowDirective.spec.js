/**
* activityWindow directive module's unit test spec
*/

describe('activityWindowDirective section', function () {
    var mockTargetTask = {
        taskId: 1,
        title: 'Test task',
        status: {id: 2, value: 'Pending Acceptance'},
        comment: 'comment text'
    };

    var scope, elm, $compile, mockHttp, metadataCollection;

    beforeEach(module('directives/activityWindow/comment.tpl.html'));
    beforeEach(module('directives/activityWindow/status.tpl.html'));
    beforeEach(module('activityWindowDirective'));
    beforeEach(module('mockMetadata'));
    beforeEach(module('dateParse'));

    beforeEach(inject(function (_$rootScope_, _$compile_, _$httpBackend_, _mockMetadataCollections_) {
        mockHttp = _$httpBackend_;
        metadataCollection = _mockMetadataCollections_;
        $compile = _$compile_;
        scope = _$rootScope_.$new();

        elm = angular.element('<div class="activityWindow"></div>');

        $compile(elm)(scope);
        scope.$digest();
    }));

    describe('DOM elements', function () {
        it('should have class', function () {
            expect(elm.hasClass('activityWindow')).toBeTruthy();
        });
    });

    describe('ActivityWindowCtrl', function () {
        var dialog,
            template = '<div>Activity window template</div>';

        describe('Add comment window', function () {
            var activityType = 'comment';
            var taskStatus = {id: 2, value: 'Pending Acceptance'};
            var mockActivity = {
                taskId: 1,
                report: 'Comment text',
                statusId: taskStatus.id,
                type: activityType
            };

            beforeEach(inject(function (_$rootScope_, _$modal_, $controller) {
                scope = _$rootScope_.$new();
                dialog = _$modal_.open({
                    template: template
                });

                mockHttp.when('GET', /metadata\.json/)
                    .respond({GetMetadataResult: {metadata: metadataCollection}});

                $controller('activityWindowCtrl', {
                    $scope: scope,
                    $modalInstance: dialog,
                    targetTask: mockTargetTask,
                    activityType: activityType
                });

                mockHttp.flush();
            }));

            it('should add a comment to a task', inject(function (tasks, dateParse) {
                var comment = {};

                expect(angular.isFunction(scope.submitComment)).toBeTruthy();

                scope.report.text = "Comment text";
                scope.$digest();

                spyOn(dialog, 'close');
                spyOn(tasks, 'addComment');
                scope.submitComment();

                expect(tasks.addComment).toHaveBeenCalled();
                // the arguments to the last addComment call
                comment = tasks.addComment.mostRecentCall.args[0];

                // test static values
                expect(_.omit(comment, ['dateReported', 'authorId'])).toEqual(mockActivity);

                // test random values
                expect(_.isDate(dateParse.fromISO(comment.dateReported))).toBeTruthy();
                expect(comment.authorId).toEqual(jasmine.any(Number));

                expect(dialog.close).toHaveBeenCalledWith();
            }));

            it('should have "close" method', function () {
                expect(angular.isFunction(scope.close)).toBeTruthy();

                spyOn(dialog, 'dismiss');
                scope.close();
                expect(dialog.dismiss).toHaveBeenCalledWith('cancel');
            });
        });

        describe('Add report window', function () {
            var activityType = 'status';
            var taskStatus = {id: 2, value: 'Pending Acceptance'};
            var mockActivity = {
                taskId: 1,
                report: 'Comment text',
                statusId: taskStatus.id,
                type: activityType
            };

            beforeEach(inject(function (_$rootScope_, _$modal_, $controller) {
                scope = _$rootScope_.$new();
                dialog = _$modal_.open({
                    template: template
                });

                mockHttp.when('GET', /metadata\.json/)
                    .respond({GetMetadataResult: {metadata: metadataCollection}});

                $controller('activityWindowCtrl', {
                    $scope: scope,
                    $modalInstance: dialog,
                    targetTask: mockTargetTask,
                    activityType: activityType
                });

                mockHttp.flush();
            }));

            it('should add a report to a task', inject(function (tasks, dateParse) {
                var comment;

                expect(angular.isFunction(scope.submitReport)).toBeTruthy();

                scope.report.text = "Comment text";
                scope.$digest();

                spyOn(dialog, 'close');
                spyOn(tasks, 'addComment');
                scope.submitReport();

                // the arguments to the last addComment call
                comment = tasks.addComment.mostRecentCall.args[0];

                // test static values
                expect(_.omit(comment, ['dateReported', 'authorId'])).toEqual(mockActivity);
                // test random values
                expect(_.isDate(dateParse.fromISO(comment.dateReported))).toBeTruthy();
                expect(comment.authorId).toEqual(jasmine.any(Number));

                expect(dialog.close).toHaveBeenCalledWith(taskStatus);
            }));
        });
    });
});
