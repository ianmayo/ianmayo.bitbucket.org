/**
 * @module activityWindowDirective
 */

angular.module('activityWindowDirective', ['ui.bootstrap', 'charsLimitDirective', 'focusOnElementDirective',
    'randomizer', 'tasks', 'restMetadata'])

.controller('activityWindowCtrl', function activityWindowController ($scope, $modalInstance, tasks,
    targetTask, activityType, restMetadata, $filter) {

    var metadataApi;
    $scope.states = [];

    restMetadata.then(function (api) {
        metadataApi = api;
        $scope.states = metadataApi.getStates();
    });

    /**
     * Activity report data
     * @returns {Object}
     */
    var getActivity = function () {
        var activity = {
            taskId: $scope.task.taskId,
            report: $scope.report.text,
            type: activityType,
            authorId: metadataApi.getCurrentUser().id,
            dateReported: $filter('date')(new Date(), 'yyyy-MM-ddTHH:mm:ss')
        };
        activity.statusId = $scope.task.status.id;

        return activity;
    };

    $scope.task = angular.copy(targetTask);
    $scope.report = {};

    /**
     * Submit status report and close window
     */
    $scope.submitReport = function () {
        var activity = getActivity();
        var taskStatus = {
            id: $scope.task.status.id,
            value: _.find($scope.states, function (state) {
                return state.id === $scope.task.status.id;
            }).value
        };

        // add activity item
        tasks.addComment(activity);

        // close the window and pass status object to update status on page
        $modalInstance.close(taskStatus);
    };

    /**
     * Submit comment and close window
     */
    $scope.submitComment = function () {
        var activity = getActivity();

        // add activity item
        tasks.addComment(activity);

        // close the window and pass status object to update status on page
        $modalInstance.close();
    };

    /**
     * Dismiss the modal and pass reason
     */
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    };
})

.directive('activityWindow', ['$modal', function ($modal) {
    var resolveTemplate = function (type) {
        return type ? 'directives/activityWindow/' + type + '.tpl.html' : '';
    };

    return {
        restrict: 'C',
        scope: {},
        link: function (scope) {
            scope.$on('showActivityWindow', function (event, options) {
                var template = resolveTemplate(options.type),
                    theTask = options.task,
                    activityWindow;

                if (template && theTask) {
                    /**
                     * Configure modal window
                     * `resolve` object passed to the controller as locals
                     * @type {*}
                     */
                    activityWindow = $modal.open({
                        templateUrl: template,
                        controller: 'activityWindowCtrl',
                        resolve: {
                            targetTask: function() {
                                return theTask;
                            },
                            activityType: function () {
                                return options.type;
                            }
                        }
                    });

                    /**
                     * Resolve promise when modal window is closed and pass a result
                     */
                    activityWindow.result.then(function changeTaskStatus(status) {
                        if (status) {
                            theTask.status = status;
                        }
                    });
                }
            });
        }
    };
}]);
