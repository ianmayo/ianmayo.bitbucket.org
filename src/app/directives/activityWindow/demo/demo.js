angular.module('DemoApp', ['tasks', 'activityWindowDirective', 'directives/activityWindow/report.tpl.html',
    'directives/activityWindow/comment.tpl.html']);

function DemoCtrl($scope, tasks) {
    $scope.task = tasks.getTasks()[1];

    $scope.openActivityWindow = function (type, task) {
        $scope.$broadcast('showActivityWindow', {type: type, task: task});
    };

    $scope.lastComment = function () {
        return $scope.task.activities;
    };
};