angular.module('DemoApp', ['workItemTypeDirective', 'directives/workItemType/workItemType.tpl.html']);

function WorkItemDemoCtrl($scope) {

    $scope.change = function () {
        $scope.workItem = {
            isDeliverable: Math.random() > 0.5,
            isTrial: Math.random() > 0.5,
            isPublication: Math.random() > 0.5,
            isFinancial: Math.random() > 0.5
        };
    };

    $scope.change();
};