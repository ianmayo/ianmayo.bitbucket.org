describe('Work Item Type directive', function () {
    var $scope, $elm, $compile;

    beforeEach(module('directives/workItemType/workItemType.tpl.html'));
    beforeEach(module('workItemTypeDirective'));

    beforeEach(inject(function(_$rootScope_, _$compile_) {
        $scope = _$rootScope_;
        $compile = _$compile_;

        $scope.workItem = {};

        $elm = angular.element('<div class="workItemType" data-item="workItem"></div>');

        $compile($elm)($scope);
        $scope.$digest();
    }));

    describe('DOM elements', function () {
        it('should have work items attribute', function () {
            expect($elm.attr('data-item')).toBeDefined();
            expect($elm.attr('data-item')).toEqual('workItem');
        });
    });

    describe('Work items types', function () {
        var $testType;

        var testAttributes = function ($elemType, iconSel, title) {
            // active button should have active class
            expect($elemType.length).toEqual(1);
            // icon should have active class
            expect($elemType.children('.icon-' + iconSel).length).toEqual(1);
            expect($elemType.attr('title')).toEqual(title);
        };

        it('should have all types inactive', function () {
            // should be all type elements
            $scope.$digest();
            expect($elm.children('a').length).toEqual(4);
            // all types should not be active
            expect($elm.children('a.active').length).toEqual(0);
        });

        it('should have active "Deliverable" type', function () {
            $scope.workItem.isDeliverable = true;
            $scope.$digest();
            $testType = $elm.children('a.active');
            testAttributes($testType, 'deliverable', 'Deliverable');

            // test model changing when click on a type icon
            $testType.click();
            expect($scope.workItem.isDeliverable).toBeFalsy();
            $testType.click();
            expect($scope.workItem.isDeliverable).toBeTruthy();
        });

        it('should have active "Trial" type', function () {
            $scope.workItem.isTrial = true;
            $scope.$digest();
            $testType = $elm.children('a.active');
            testAttributes($testType, 'trial', 'Trial');

            // test model changing when click on a type icon
            $testType.click();
            expect($scope.workItem.isTrial).toBeFalsy();
            $testType.click();
            expect($scope.workItem.isTrial).toBeTruthy();
        });

        it('should have active "Publication" type', function () {
            $scope.workItem.isPublication = true;
            $scope.$digest();
            $testType = $elm.children('a.active');
            testAttributes($testType, 'publication', 'Publication');

            // test model changing when click on a type icon
            $testType.click();
            expect($scope.workItem.isPublication).toBeFalsy();
            $testType.click();
            expect($scope.workItem.isPublication).toBeTruthy();
        });

        it('should have active "Financial" type', function () {
            $scope.workItem.isFinancial = true;
            $scope.$digest();
            $testType = $elm.children('a.active');
            testAttributes($testType, 'finance', 'Finance');

            // test model changing when click on a type icon
            $testType.click();
            expect($scope.workItem.isFinancial).toBeFalsy();
            $testType.click();
            expect($scope.workItem.isFinancial).toBeTruthy();
        });
    });
});

describe('Work Item Type static directive', function () {
    var $scope, $elm, $compile;

    beforeEach(module('workItemTypeDirective'));

    beforeEach(inject(function(_$rootScope_, _$compile_) {
        $scope = _$rootScope_;
        $compile = _$compile_;

        $scope.workItem = {};

        $elm = angular.element('<div class="workItemTypeStatic" data-item="workItem"></div>');

        $compile($elm)($scope);
        $scope.$digest();
    }));

    describe('DOM elements', function () {
        it('should have work items attribute', function () {
            expect($elm.attr('data-item')).toBeDefined();
            expect($elm.attr('data-item')).toEqual('workItem');
        });
    });

    describe('Work items types', function () {
        var testAttributes = function ($elm, iconSel) {
            // icon should have type class
            expect($elm.children('.icon-' + iconSel).length).toEqual(1);
            // icon should be visible
            expect($elm.children('.icon-' + iconSel)[0].style.display).toEqual('');
        };

        it('should have all types hidden', function () {
            // should be all type elements
            $scope.$digest();
            expect($elm.children('i').length).toEqual(4);
            // all types should not be visible
            expect($elm.children('i:visible').length).toEqual(0);
        });

        it('should have active "Deliverable" type', function () {
            $scope.workItem.isDeliverable = true;
            $scope.$digest();
            testAttributes($elm, 'deliverable');
        });

        it('should have active "Trial" type', function () {
            $scope.workItem.isTrial = true;
            $scope.$digest();
            testAttributes($elm, 'trial');
        });

        it('should have active "Publication" type', function () {
            $scope.workItem.isPublication = true;
            $scope.$digest();
            testAttributes($elm, 'publication');
        });

        it('should have active "Financial" type', function () {
            $scope.workItem.isFinancial = true;
            $scope.$digest();
            testAttributes($elm, 'finance');
        });
    });
});
