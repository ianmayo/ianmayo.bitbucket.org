/**
 * @module WorkItemType directive
 */

angular.module('workItemTypeDirective', [])

.controller('workItemTypeController', ['$scope', function($scope) {
    /**
     * Possible work item types
     * @type {Array}
     */
    $scope.types = [{
            name: 'isDeliverable',
            title: 'Deliverable'
        }, {
            name: 'isTrial',
            title: 'Trial'
        }, {
            name: 'isPublication',
            title: 'Publication'
        }, {
            name: 'isFinancial',
            title: 'Finance'
        }
    ];

    /**
     * Returns state of a work item type
     *
     * @param {String} type A type key
     * @returns {*}
     */
    $scope.isActive = function (type) {
        return $scope.item[type];
    };

    /**
     * Changes state of a work item type to opposite value
     *
     * @param {String} type A type key
     */
    $scope.change = function (type) {
        $scope.item[type] = !$scope.item[type];
    };
}])

.directive('workItemType', function () {
    return {
        restrict: 'C',
        scope: {
            item: '='
        },
        replace: false,
        controller: 'workItemTypeController',
        templateUrl: 'directives/workItemType/workItemType.tpl.html'
    };
})

.directive('workItemTypeStatic', function () {
    return {
        restrict: 'C',
        scope: {
            item: '='
        },
        replace: false,
        controller: 'workItemTypeController',
        template: '<i class="icon-sprite icon-{{type.title | lowercase}}" data-ng-repeat="type in types" ' +
            'data-ng-show="isActive(type.name)"></i>'
    };
});
