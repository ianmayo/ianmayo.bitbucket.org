/**
 * Guidance directive module's unit test spec
 */

describe('guidanceDirective section', function () {

    var scope, elm, $compile;

    beforeEach(module('guidanceDirective'));

    beforeEach(module('ui.bootstrap.tooltip', function ($tooltipProvider) {
        $tooltipProvider.options({animation: false});
    }));

    beforeEach(inject(function(_$rootScope_, _$compile_, $injector) {
        $compile = _$compile_;
        scope = _$rootScope_;

        elm = angular.element(
            '<div class="container">' +
                '<input data-guidance="Help text">' +
            '</div>');

        $compile(elm)(scope);

        scope.$digest();
    }));

    describe('DOM elements', function () {
        it('should have element with data-guidance attribute', function () {
            expect(elm.find('[data-guidance]').length).toBe(1);
        });

        it('should have "?" icon', function () {
            expect(elm.find('.icon2-question-sign').length).toBe(1);
        });
    });

    describe('Guidance directive', function () {
        it('should show/hide tooltip when element is focused/blur', inject(function($timeout) {
            expect(elm.find('.tooltip').length).toBe(0);

            elm.find('input').focus();
            $timeout.flush();

            expect(elm.find('.tooltip').length).toBe(1);

            elm.find('input').blur();
            $timeout.flush();

            expect(elm.find('.tooltip').length).toBe(0);
        }));
    });

    describe('Tooltip', function () {
        it('should be blocked when guidance element is focused', inject(function($timeout) {
            expect(elm.find('.tooltip').length).toBe(0);

            elm.find('input').focus();
            $timeout.flush();
            expect(elm.find('.tooltip').length).toBe(1);

            elm.find('.icon2-question-sign').mouseenter();
            expect(elm.find('.tooltip').length).toBe(1);

            elm.find('.icon2-question-sign').mouseleave();
            expect(elm.find('.tooltip').length).toBe(1);
        }));

        it('should be shown/hidden when guidance element isn\'t focused', inject(function($timeout) {
            expect(elm.find('.tooltip').length).toBe(0);

            elm.find('input').focus();
            $timeout.flush();

            expect(elm.find('.tooltip').length).toBe(1);

            elm.find('input').blur();
            $timeout.flush();

            expect(elm.find('.tooltip').length).toBe(0);

            elm.find('.icon2-question-sign').mouseenter();
            $timeout.flush();

            expect(elm.find('.tooltip').length).toBe(1);

            elm.find('.icon2-question-sign').mouseleave();
            $timeout.flush();

            expect(elm.find('.tooltip').length).toBe(0);
        }));
    });
});
