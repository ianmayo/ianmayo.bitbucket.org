/**
 * @module Guidance directive
 *
 * This directive strongly depends on tooltipDirective and tooltipWatchDirective
 * the aim of the directive is implementing of "guidance" functionality and behavior using its dependencies
 */

angular.module('guidanceDirective', ['tooltipWatchDirective'])

.factory('guidanceConfig', function () {
    return {
        addIcon: function (field, tooltip) {
            field.after(tooltip);
        }
    };
})

.directive('guidance', function ($compile, $rootScope, guidanceConfig) {

    /**
     * Help icon "?" template
     *
     * @type {Function}
     */
    var helpIconTemplate = _.template(
        '<i class="icon2-question-sign" ' +
            'data-tooltip-watch="isTooltipShown" ' +
            'data-tooltip-trigger="showTooltip" ' +
            'data-tooltip-placement="<%= placement %>" ' +
            'data-tooltip="<%= guidance %>">' +
        '</i>'
    );

    return {
        restrict: 'A',
        replace: false,

        link: function (scope, element, attributes) {

            var field = angular.element(element[0]);

            /**
             * Create new scope for tooltipWatch and tooltip directives
             * Why we do this because we need to access this scope in guidance directive
             * to manage defined "guidance" functionality and we don't want to
             * create any additional scope's properties/variables in controller
             *
             * @type {*|Object}
             */
            var $scope = $rootScope.$new();

            $scope.isTooltipShown = false;
            $scope.isElementFocused = false;

            /**
             * Compile tooltip element with two directives (tooltip, tooltipWatch)
             *
             * @type {*}
             */
            var tooltip = $compile(helpIconTemplate({
                guidance: attributes.guidance,
                placement: attributes.guidancePlacement || 'right'
            }))($scope);

            /**
             * Focus event handler on element with guidance
             */
            field.bind('focus', function () {
                $scope.$apply(function () {
                    $scope.isTooltipShown = $scope.isElementFocused = true;
                });
            });

            /**
             * Blur event handler on element with guidance
             */
            field.bind('blur', function () {
                $scope.$apply(function () {
                    $scope.isTooltipShown = $scope.isElementFocused = false;
                });
            });

            /**
             * Mouseenter event handler on guidance "?" icon, it works only when element isn't focused
             */
            tooltip.bind('mouseenter', function () {
                $scope.$apply(function () {
                    if (!$scope.isElementFocused) {
                        $scope.isTooltipShown = true;
                    }
                });
            });

            /**
             * Mouseleave event handler on guidance "?" icon, it works only when element isn't focused
             */
            tooltip.bind('mouseleave', function () {
                $scope.$apply(function () {
                    if (!$scope.isElementFocused) {
                        $scope.isTooltipShown = false;
                    }
                });
            });

            /**
             * Add compiled "?" guidance icon to the DOM right after the element
             */
            guidanceConfig.addIcon(field, tooltip);
        }
    };
});
