/**
 * Remove Work Item directive module's unit test spec
 */

describe('Remove Work Item Directive', function () {

    var $scope, $elm, $compile;

    beforeEach(module('removeWorkItemDirective'));

    beforeEach(inject(function(_$rootScope_, _$compile_) {
        $scope = _$rootScope_;
        $compile = _$compile_;

        $scope.task = {
            index: 0,
            items: [{
                name: '',
                start: new Date(),
                finish: new Date(),
                percent: 0,
                people: [],
                description: ''
            }]
        };

        $elm = angular.element('<a class="removeWorkItem" data-index="index" data-items="task.items">');

        $compile($elm)($scope);
        $scope.$digest();
    }));

    describe('DOM elements', function () {
        it('should have work items attribute', function () {
            expect($elm.attr('data-index')).toBeDefined();
            expect($elm.attr('data-items')).toBeDefined();
            expect($elm.attr('data-items')).toEqual('task.items');
        });
    });

    describe('Work items collection', function () {
        it('should have non empty work items list', function () {
            expect($scope.task.items.length).toEqual(1);
        });

        it('should have empty work items list', function () {
            $elm.click();
            expect($scope.task.items.length).toEqual(0);
        });
    });
});
