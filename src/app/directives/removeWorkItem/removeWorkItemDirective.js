/**
 * @module Remove Work Item directive
 */

angular.module('removeWorkItemDirective', [])

.directive('removeWorkItem', function () {
    return {
        restrict: 'C',
        replace: false,

        scope: {
            index: "@",
            items: "="
        },

        link: function (scope, elm) {

            elm.bind('click', function () {
                scope.$apply(function () {
                    scope.items.splice(scope.index, 1);
                });
            });
        }
    };
});
