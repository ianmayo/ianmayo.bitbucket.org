/**
 * timeBarAxis directive module's unit test spec
 */

describe('timeBarAxisDirective section', function () {

    var scope, elm, $compile, dateParseService;

    beforeEach(module('directives/timeBarAxis/timeBarAxis.tpl.html'));
    beforeEach(module('timeBarAxisDirective'));
    beforeEach(module('dateParse'));

    beforeEach(inject(function(_$rootScope_, _$compile_, dateParse) {
        
        $compile = _$compile_;
        scope = _$rootScope_;
        dateParseService = dateParse;
        scope.timeAxis = {
            months: 1,
            start: dateParseService.fromISO('2013-06-01')
        };
        scope.task = {isTaskCollapsed: false};

        elm = angular.element(
            '<div class="timeBarAxis" data-axis="timeAxis"' +
            ' data-is-collapsed="task.isTaskCollapsed"></div>'
        );

        $compile(elm)(scope);
        scope.$digest();
    }));

    it('should have DOM elements', function () {
        expect(elm.hasClass('timeBarAxis')).toBeTruthy();
        expect(elm.attr('data-axis')).toEqual('timeAxis');

        expect(elm.children().length).toEqual(1);
        expect(elm.children().hasClass('axisColumn')).toBeTruthy();

        // one month scale, just for children element count
        expect(elm.children().children().length).toEqual(2);
        expect(elm.children().children().eq(0).hasClass('columnMonth')).toBeTruthy();
        expect(elm.children().children().eq(1).hasClass('columnBar')).toBeTruthy();
    });


    describe('Change months axis scale', function () {
        var months, columnAxis;
        it('should have 3 axis columns', function () {
            months = 3;
            scope.timeAxis.months = months;
            scope.$digest();
            columnAxis = elm.children();
            expect(columnAxis.length).toEqual(months);
        });

        it('should have 6 axis columns', function () {
            months = 6;
            scope.timeAxis.months = months;
            scope.$digest();
            columnAxis = elm.children();
            expect(columnAxis.length).toEqual(months);
        });

        it('should have 12 axis columns', function () {
            months = 12;
            scope.timeAxis.months = months;
            scope.$digest();
            columnAxis = elm.children();
            expect(columnAxis.length).toEqual(months);
        });
    });

    describe('Change origin date', function () {
        var columnMonth, monthList;
        beforeEach(inject(function($locale) {
            monthList = $locale.DATETIME_FORMATS.SHORTMONTH;
        }));

        it('should have full year boundary', function () {
            scope.timeAxis = {
                months: 12,
                start: dateParseService.fromISO('2013-01-01')
            };
            scope.$digest();
            columnMonth = elm.children().eq(0).children().html();
            expect(columnMonth).toEqual(monthList[0]);
            columnMonth = elm.children().eq(11).children().html();
            expect(columnMonth).toEqual(monthList[11]);
        });

        it('should have first quarter boundary', function () {
            scope.timeAxis = {
                months: 3,
                start: dateParseService.fromISO('2013-01-01')
            };
            scope.$digest();
            columnMonth = elm.children().eq(0).children().html();
            expect(columnMonth).toEqual(monthList[0]);
            columnMonth = elm.children().eq(2).children().html();
            expect(columnMonth).toEqual(monthList[2]);
        });

        it('should have first six-months boundary', function () {
            scope.timeAxis = {
                months: 6,
                start: dateParseService.fromISO('2013-01-01')
            };
            scope.$digest();
            columnMonth = elm.children().eq(0).children().html();
            expect(columnMonth).toEqual(monthList[0]);
            columnMonth = elm.children().eq(5).children().html();
            expect(columnMonth).toEqual(monthList[5]);
        });

        it('should render next year months', function () {
            scope.timeAxis = {
                months: 12,
                start: dateParseService.fromISO('2013-08-01')
            };
            scope.$digest();
            columnMonth = elm.children().eq(0).children().html();
            expect(columnMonth).toEqual(monthList[7]);
            columnMonth = elm.children().eq(11).children().html();
            expect(columnMonth).toEqual(monthList[6]);
        });
    });

    describe('Elements "axisColumn"', function () {
        var period;

        it('should change "axisColumn" width', function () {
            var elAxisColumn;
            period = 3;
            scope.timeAxis.months = period;
            scope.$digest();
            elAxisColumn = elm.children().eq(0);
            expect(elAxisColumn.css('width')).toEqual((100 / period).toFixed(2) + '%');

            period = 6;
            scope.timeAxis.months = period;
            scope.$digest();
            elAxisColumn = elm.children().eq(0);
            expect(elAxisColumn.css('width')).toEqual((100 / period).toFixed(2) + '%');

            period = 12;
            scope.timeAxis.months = period;
            scope.$digest();
            elAxisColumn = elm.children().eq(0);
            expect(elAxisColumn.css('width')).toEqual((100 / period).toFixed(2) + '%');
        });
    });

    describe('TimeBarAxis collapsing', function () {
        var htmlContent;

        it('should have collapsed attribute', function () {
            expect(elm.attr('data-is-collapsed')).toBeDefined();
        });

        it('should not change html content when work items are collapsed', function () {
            htmlContent = elm.html();

            // set task collapsed
            scope.task.isTaskCollapsed = true;

            // change start date and scale
            scope.timeAxis = {
                months: 3,
                start: dateParseService.fromISO('2013-01-01')
            };
            scope.$digest();
            expect(elm.html()).toEqual(htmlContent);

            // and change start date and scale
            scope.timeAxis = {
                months: 6,
                start: dateParseService.fromISO('2013-02-01')
            };
            scope.$digest();
            expect(elm.html()).toEqual(htmlContent);
        });

        it('should change html content when work items are expanded', function () {
            // get html content
            htmlContent = elm.html();

            // set task expanded
            scope.task.isTaskCollapsed = false;

            // change start date and scale
            scope.timeAxis = {
                months: 3,
                start: dateParseService.fromISO('2013-01-01')
            };
            scope.$digest();
            expect(elm.html()).not.toEqual(htmlContent);

            // get new html content
            htmlContent = elm.html();
            // and change start date and scale
            scope.timeAxis = {
                months: 6,
                start: dateParseService.fromISO('2013-02-01')
            };
            scope.$digest();
            expect(elm.html()).not.toEqual(htmlContent);
        });
    });
});
