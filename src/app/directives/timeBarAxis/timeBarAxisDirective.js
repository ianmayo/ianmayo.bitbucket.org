/**
 * @module timeBarAxisDirective
 */

angular.module('timeBarAxisDirective', [])

.directive('timeBarAxis', ['$locale', function ($locale) {

    // Provided that all timeBarAxis directives use the same scale and start date
    // it is possible to use cached values. 
    var axisScale,
        axisStartDate,
        axisMonthsList;
        
    var monthsList = $locale.DATETIME_FORMATS.SHORTMONTH;
    var monthColumns = function (scale, originDate) {
        var currentMonth,
            anotherYearMonths;

        if (axisScale === scale && originDate.getTime() === axisStartDate) {
            // return cached list
            return axisMonthsList;
        } else {
            // add values to cache
            axisScale = scale;
            axisStartDate = originDate.getTime();
        }

        currentMonth = originDate.getMonth();

        // extract months which fit into scale period
        axisMonthsList = monthsList.slice(currentMonth, scale + currentMonth);

        if (axisMonthsList.length < scale) {
            // add months of a prev/next year
            anotherYearMonths = monthsList.slice(0, scale - axisMonthsList.length);
            axisMonthsList = axisMonthsList.concat(anotherYearMonths);
        }
        return axisMonthsList;
    };

    return {
        restrict: 'CA',
        replace: false,
        scope: {
            axis: '=',
            isCollapsed: '='
        },
        templateUrl: 'directives/timeBarAxis/timeBarAxis.tpl.html',
        compile: function () {
            return function postLink(scope) {
                scope.months = 0;
                scope.monthWidth = 0;

                scope.$watch(function axisChanges() {
                    if (scope.isCollapsed) {
                        // work items are collapsed, skip renders
                        return false;
                    } else {
                        // render axis
                        return scope.axis.months + scope.axis.start.getTime();
                    }
                }, function (axisChanged) {
                    if (!axisChanged) {
                        // it was probably collapsed, drop it out
                        return;
                    } else {
                        scope.months = monthColumns(scope.axis.months, scope.axis.start);
                        scope.monthWidth = (100 / scope.axis.months).toFixed(2) + '%';
                    }
                });
            };
        }
    };
}]);