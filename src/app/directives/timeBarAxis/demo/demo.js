angular.module('DemoApp', ['ui.bootstrap', 'timeBarDirective', 'timeBarAxisDirective',
    'directives/timeBarAxis/timeBarAxis.tpl.html']);

function DemoCtrl($scope, $locale) {

    $scope.datePicker = new Date('2013/07/01'); // IE 8 date
    $scope.timeBars = [
        {
            percent: 40,
            item: {
                startDate: '2013-07-01',
                endDate: '2013-08-01'
            }
        },{
            percent: 50,
            item: {
                startDate: '2013-08-01',
                endDate: '2013-09-01'
            }
        },{
            percent: 80,
            item: {
                startDate: '2013-09-01',
                endDate: '2013-12-01'
            }
        }
    ];

    $scope.timeAxis = {
        scale: 6,
        originDate: $scope.datePicker
    };

    $scope.today = function() {
        $scope.datePicker = new Date();
    };

    $scope.months = $locale.DATETIME_FORMATS.SHORTMONTH;

    $scope.deleteItem = function(index) {
        $scope.timeBars.pop(index);
    };

    $scope.addItem = function () {
        var item = {
            percent: 40,
            item: {
                startDate: '2013-05-01',
                endDate: '2013-08-01'
            }
        };
        $scope.timeBars.push(item);
    }

};