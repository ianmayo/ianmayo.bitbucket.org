/**
 * @module Tooltip Watch directive
 * This is the helper directive which helps us to control and switch
 * view state of angular-bootstrap tooltip directive
 */

angular.module('tooltipWatchDirective', ['ui.bootstrap'])

.config(function ($tooltipProvider) {

    $tooltipProvider.setTriggers({
        'showTooltip': 'hideTooltip'
    });
})

.directive('tooltipWatch', function ($timeout) {

    return {
        restrict: 'A',
        replace: false,

        link: function (scope, element, attributes) {

            /**
             * When tooltipWatch attribute is changed it switches tooltip view state
             * from hidden to show and vv
             */
            scope.$watch(attributes.tooltipWatch, function (newValue) {

                /**
                 * We use $timeout here to prevent "$digest in progress" error
                 * it's waiting while $digest do its work and then run next code
                 */
                $timeout(function () {
                    element.trigger(newValue ? 'showTooltip' : 'hideTooltip');
                });
            });
        }
    };
});
