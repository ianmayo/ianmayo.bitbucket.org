/**
 * Tooltip Watch directive module's unit test spec
 */

describe('tooltipWatchDirective section', function () {

    var scope, elm, $compile, timeout;

    beforeEach(module('tooltipWatchDirective'));

    beforeEach(module('ui.bootstrap.tooltip', function ($tooltipProvider) {
        $tooltipProvider.options({animation: false});
    }));

    beforeEach(inject(function(_$rootScope_, _$compile_){
        $compile = _$compile_;
        scope = _$rootScope_;

        scope.isTooltipShown = false;

        elm = angular.element(
        '<div class="container">' +
            '<i class="icon2-question-sign" ' +
                'data-tooltip-watch="isTooltipShown" ' +
                'data-tooltip-trigger="showTooltip" ' +
                'data-tooltip="Help text">' +
            '</i>' +
        '</div>');

        $compile(elm)(scope);
        scope.$digest();
    }));

    describe('Element with tooltip', function () {
        it('should exist', function () {
            expect(elm.find('.icon2-question-sign')).toBeDefined();
        });
    });

    describe('Tooltip', function () {
        it('should be toggled by provided isTooltipShown variable', inject(function($timeout) {
            expect(elm.find('.tooltip').length).toBe(0);

            scope.$apply(function() {
                scope.isTooltipShown = true;
            });

            $timeout.flush();

            expect(elm.find('.tooltip').length).toBe(1);

            scope.$apply(function() {
                scope.isTooltipShown = false;
            });

            $timeout.flush();

            expect(elm.find('.tooltip').length).toBe(0);
        }));
    });
});
