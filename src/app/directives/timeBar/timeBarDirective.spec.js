describe('timeBarDirective', function () {

    var scope, elm, $compile, dateParseService;

    beforeEach(module('template/progressbar/progress.html', 'template/progressbar/bar.html'));
    beforeEach(module('directives/timeBar/timeBar.tpl.html'));
    beforeEach(module('timeBarDirective'));
    beforeEach(module('dateParse'));


    beforeEach(inject(function(_$rootScope_, _$compile_, dateParse){
        $compile = _$compile_;
        scope = _$rootScope_;
        dateParseService = dateParse;
        scope.timeBar = {
            percent: 40,
            timeAxis: {months: 3, start: dateParseService.fromISO('2013-06-01')},
            item: {
                startDate: '2013-06-01',
                finishDate: '2013-07-01'
            }
        };
        scope.task = {isTaskCollapsed: false};

        var periodEnd;
        periodEnd = new Date(scope.timeBar.timeAxis.start);
        periodEnd.setDate(1);
        periodEnd.setHours(0, 0, -1);
        periodEnd.setMonth(periodEnd.getMonth() + scope.timeBar.timeAxis.months);
        scope.timeBar.timeAxis.end = periodEnd;


        elm = angular.element(
            '<div>' +
            '   <div class="canvas" style="width: 100px;">' +
            '       <div class="timeBar" style="width: 80px; position: relative"' +
            '           data-complete-percent="timeBar.percent"' +
            '           data-item="timeBar.item"' +
            '           data-time-axis="timeBar.timeAxis"' +
            '           data-is-collapsed="task.isTaskCollapsed">' +
            '       </div>' +
            '   </div>' +
            '</div>'
        );

        $compile(elm)(scope);
        scope.$digest();
    }));

    describe('DOM elements', function () {
        var progress, indent, canvas;

        it('should have canvas wrapper with 100px width', function () {
            canvas = elm.children();
            expect(canvas.hasClass('canvas')).toBeTruthy();
            expect(canvas.length).toBe(1);
            expect(canvas.css('width')).toBe('100px');
        });

        it('should have indent container', function () {
            indent = elm.children().children().children();
            expect(indent.hasClass('startDateOffset')).toBeTruthy();
            expect(indent.length).toBe(1);
        });

        it('should contain one child element as a progress', function () {
            progress = elm.children().children().children().children();
            expect(progress.children().eq(0).hasClass('progress')).toBeTruthy();
            expect(progress.length).toBe(1);
        });
    });

    describe('Working cycles', function () {
        var start = '2013-08-15';
        var finish = '2013-08-01';

        it('item should react on visibility', function () {

            // if an item is collapsed (a task is collapsed), the directive should not change data of the item
            scope.$apply(function () {
                // set task collapsed
                scope.task.isTaskCollapsed = true;
                // and change item dates
                scope.timeBar.item = {
                    startDate: start,
                    finishDate: finish
                };
            });

            // the dates should be the same (initial state)
            expect(start).toEqual(scope.timeBar.item.startDate);
            expect(finish).toEqual(scope.timeBar.item.finishDate);

            scope.$apply(function () {
                // set task expanded, data should be changed
                scope.task.isTaskCollapsed = false;
            });

            // the dates should be another (converted from initial state)
            expect(start).not.toEqual(scope.timeBar.item.start);
            expect(finish).not.toEqual(scope.timeBar.item.finish);
        });

        it('should convert start and finish dates from string to Date object', function () {
            scope.$apply(function () {
                scope.task.isTaskCollapsed = true;
                scope.timeBar.item = {
                    startDate: start,
                    finishDate: finish
                };
            });

            scope.$apply(function () {
                // expand task
                scope.task.isTaskCollapsed = false;
            });

            // start date
            expect(scope.timeBar.item.startDate).toBeDefined();
            expect(angular.isDate(scope.timeBar.item.startDate)).toBeTruthy();
            expect(scope.timeBar.item.startDate.getTime()).toEqual(dateParseService.fromISO(start).getTime());

            // finish date
            expect(scope.timeBar.item.finishDate).toBeDefined();
            expect(angular.isDate(scope.timeBar.item.finishDate)).toBeTruthy();
            expect(scope.timeBar.item.finishDate.getTime()).toEqual(dateParseService.fromISO(finish).getTime());
        });
    });

    describe('Progress element "complete" width', function () {
        var progress;

        it('should have progress bar with right width', function() {
            progress = elm.children().children().children().children();
            expect(progress.children().children().eq(0).css('width')).toBe('40%');
        });

        it('should change width when percent value changes', function () {
            scope.$apply(function () {
                scope.timeBar.percent = 60;
            });
            progress = elm.children().children().children().children();
            expect(progress.children().children().eq(0).css('width')).toBe('60%');
        });
    });

    describe('Progress element width', function () {
        var progress;
        //maths to calculate the correct width using the dates & screen width

        it('should have "timeAxis" attribute', function () {
            expect(elm.children().children().attr('data-time-axis')).toBe('timeBar.timeAxis');
        });

        it('should have "item" attribute', function () {
            expect(elm.children().children().attr('data-item')).toBe('timeBar.item');
        });

        it('should have width', function () {
            progress = elm.children().children().children().children();
            expect(progress.css('width')).toBe('33px');
        });
    });

    describe('Left indent', function () {
        var indent;

        it('should have zero ', function () {
            indent = elm.children().children().children();
            expect(indent.css('left')).toBe('0px');
        });

        it('should change offset', function () {
            scope.$apply(function () {
                scope.timeBar.timeAxis.start = dateParseService.fromISO('2013-07-01');
            });
            indent = elm.children().children().children();
            expect(indent.css('left')).toBe('-33px');
        });
    });

    describe('Time-period change', function () {
        var progress, indent;

        it('should change months', function () {
            scope.$apply(function () {
                scope.timeBar.timeAxis.months = 6;
            });
            progress = elm.children().children().children().children();
            expect(progress.css('width')).toBe('16px');
        });

        it('should change offset', function () {
            scope.$apply(function () {
                scope.timeBar.timeAxis.months = 6;
                scope.timeBar.timeAxis.start = dateParseService.fromISO('2013-07-01');
            });
            indent = elm.children().children().children();
            expect(indent.css('left')).toBe('-16px');
        });
    });

    describe('Item date-period is invalid', function () {
        var progress;

        it('finish date is less start date', function () {
            scope.$apply(function () {
                scope.timeBar.timeAxis.start = dateParseService.fromISO('2013-07-01');
                scope.timeBar.item = {
                    startDate: '2013-08-15',
                    finishDate: '2013-08-01'
                };
            });
            progress = elm.children().children().children().children();
            expect(progress.css('width')).toBe('0px');
        });

        it('start and finish dates are equal', function () {
            scope.$apply(function () {
                scope.timeBar.timeAxis.start = dateParseService.fromISO('2013-07-01');
                scope.timeBar.item = {
                    startDate: '2013-07-15',
                    finishDate: '2013-07-15'
                };
            });
            progress = elm.children().children().children().children();
            expect(progress.css('width')).toBe('0px');
        });
    });

    describe('Progress visibility', function () {
        var barWrap;

        it('should have "item" attribute', function () {
            expect(elm.children().children().attr('data-is-collapsed')).toBeDefined();
        });

        it('should be visible when item is not collapsed', function () {
            barWrap = elm.children().children().children();
            expect(barWrap.hasClass('visible')).toBeTruthy();
        });

        it('should be invisible when item is collapsed', function () {
            scope.$apply(function () {
                scope.task.isTaskCollapsed = true;
            });
            barWrap = elm.children().children().children();
            expect(barWrap.hasClass('visible')).toBeFalsy();
        });

        it('should be invisible when item is outside of canvas', function () {
            // an item is behind of canvas
            scope.$apply(function () {
                scope.timeBar.timeAxis.start = dateParseService.fromISO('2013-07-01');
                scope.timeBar.item = {
                    startDate: '2013-05-01',
                    finishDate: '2013-06-30'
                };
            });
            barWrap = elm.children().children().children();
            expect(barWrap.hasClass('visible')).toBeFalsy();

            // an item is ahead of canvas
            scope.$apply(function () {
                scope.timeBar.timeAxis.months = 3;
                scope.timeBar.timeAxis.start = dateParseService.fromISO('2013-07-01');
                scope.timeBar.item = {
                    startDate: '2013-09-01',
                    finishDate: '2013-11-01'
                };
            });
            barWrap = elm.children().children().children();
            expect(barWrap.hasClass('visible')).toBeFalsy();
        });
    });

});
