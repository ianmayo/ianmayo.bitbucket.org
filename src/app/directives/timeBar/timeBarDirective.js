/**
 * The interval between Start date and End date can be expressed in millis
 * Start date in millis, and a time period in millis
 * The screen width occupied by the time bars.
 * Start position of the bar is the "latest of" the start of that work item, and the start-time for the visible period.
 * End position of the bar is the "earliest of" the end date for the work item, and hte end-time for the visible period.
 * The scale is fixed by the drop-down ("6 months")
 * The multiplication factor is a combination fo the time-period from the drop down (millis), and the chart width (px)
 * The start date for the "chart" is the first of the current month
 * Support through <-  and -> arrows next to the "6 months" drop-down.
 */
angular.module('timeBarDirective', ['ui.bootstrap', 'dateParse'])

.constant('timeBarConfig', {
    timeAxis: {
        scale: 3,
        originDate: new Date()
    },
    millisOneDay: 1000 * 60 * 60 * 24
})

.directive('timeBar', ['$rootScope', 'timeBarConfig', 'dateParse', function ($rootScope, timeBarConfig, dateParse) {

    // Provided that all timeBar directives use the same scale and start date
    // it is possible to use cached values.
    var canvasDate = {},
        canvasWidth,
        canvasPeriodMonths,
        canvasScaleDays;

    var canvasPixelWidth = function (canvas) {
            if (!canvasWidth) {
                canvasWidth = canvas[0].style.width;
                if (!canvasWidth) {
                    canvasWidth = canvas[0].clientWidth;
                }
                canvasWidth = parseInt(canvasWidth, 10);
            }
            return canvasWidth;
        },
        setCanvasDatePeriod = function (value) {
            canvasDate.start = value;

            canvasDate.end = new Date(canvasDate.start);
            canvasDate.end.setMonth(canvasDate.start.getMonth() + canvasPeriodMonths);

            canvasDate.startTimeVal = canvasDate.start.getTime();
            canvasDate.endTimeVal = canvasDate.end.getTime();
        },
        canvasDaysLength = function () {
            return (canvasDate.endTimeVal - canvasDate.startTimeVal) / timeBarConfig.millisOneDay;
        };

    /**
     * The function changes/adds necessary data of an item.
     * It can be used to process a data like server response.
     *
     * @param task {Object}
     * @returns {Array}
     */
    var processItem = function (item) {
        if (angular.isDate(item.startDate)) {
            // an dates were converted
            return item;
        }

        item.startDate = dateParse.fromISO(item.startDate);
        item.finishDate = dateParse.fromISO(item.finishDate);
        return item;
    };

    return {
        restrict: 'C',
        replace: false,
        scope: {
            percent: '=completePercent',
            timeAxis: '=',
            item: '=',
            isCollapsed: '='
        },
        controller: function ($scope, $element) {
            this.itemOutOfCanvas = function (t1, t2) {
                var skipItem = false;
                if ((t1.getTime() - t2.getTime()) > 0) {
                    skipItem = true;
                }
                return skipItem;
            };

            this.calcDaysInPixel = function () {
                if (canvasPeriodMonths === $scope.timeAxis.months &&
                        $scope.timeAxis.start.getTime() === canvasDate.startTimeVal) {
                    // a timeBar uses cached values which were calculated in previous cycle or by another timeBar
                    return;
                }

                // otherwise calculate new values and add them to cache
                canvasPeriodMonths = $scope.timeAxis.months;
                setCanvasDatePeriod($scope.timeAxis.start);
                canvasScaleDays = timeBarConfig.millisOneDay / (canvasPixelWidth($element.parent()) / canvasDaysLength());
            };

            this.daysInItem = function () {
                return ($scope.item.finishDate.getTime() - $scope.item.startDate.getTime()) /
                        canvasScaleDays;
            };

            this.daysOffset = function () {
                return ($scope.item.startDate.getTime() - $scope.timeAxis.start.getTime()) /
                        canvasScaleDays;
            };
        },
        templateUrl: 'directives/timeBar/timeBar.tpl.html',
        compile: function () {
            return function postLink(scope, elm, attrs, controller) {
                scope.timeAxis = angular.isDefined(scope.timeAxis) ? scope.timeAxis : timeBarConfig.timeAxis;
                scope.elmVisible = false;

                scope.$watch(function itemChanges() {
                    if (scope.isCollapsed) {
                        // an item is invisible (collapsed), skip renders
                        return false;
                    } else {
                        // show item
                        return scope.timeAxis.start + scope.timeAxis.months.toString();
                    }

                }, function (itemChanged) {
                    if (!itemChanged) {
                        // it was probably invisible, hide it & drop out
                        scope.elmVisible = false;
                        return;
                    }

                    scope.item = processItem(scope.item);

                    if (controller.itemOutOfCanvas(scope.timeAxis.start, scope.item.finishDate)) {
                        // an item is behind of canvas, skip render
                        scope.elmVisible = false;
                    } else if (controller.itemOutOfCanvas(scope.item.startDate, scope.timeAxis.end)) {
                        // an item is ahead of canvas, skip render
                        scope.elmVisible = false;
                    } else {
                        // ok, it's not collapsed, and it's in the time period. Show it.
                        scope.elmVisible = true;

                        // and sort out the left/width
                        controller.calcDaysInPixel();
                        scope.elmLeftOffset = Math.round(controller.daysOffset());
                        scope.progressWidth = Math.round(controller.daysInItem());

                        if (scope.progressWidth < 0) {
                            scope.progressWidth = 0;
                        }
                    }
                });
            };
        }
    };
}]);
