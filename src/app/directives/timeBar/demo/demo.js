angular.module('DemoApp', ['ui.bootstrap', 'timeBarDirective', 'directives/timeBar/timeBar.tpl.html']);

function ProgressDemoCtrl($scope) {

    $scope.datePicker = new Date('2013/05/01'); // IE 8 date
    $scope.timeBars = [
        {
            percent: 40,
            timeAxis: {months: 3, start: $scope.datePicker},
            item: {
                startDate: '2013-05-01',
                finishDate: '2013-08-01'
            }
        },{
            percent: 40,
            timeAxis: {months: 6, start: $scope.datePicker},
            item: {
                startDate: '2013-05-01',
                finishDate: '2013-08-01'
            }
        },{
            percent: 40,
            timeAxis: {months: 12, start: $scope.datePicker},
            item: {
                startDate: '2013-05-01',
                finishDate: '2013-08-01'
            }
        }
    ];

    $scope.today = function() {
        $scope.datePicker = new Date();
    };

    $scope.$watch('datePicker', function (newVal) {
        angular.forEach($scope.timeBars, function (timeBar) {
            var endOfPeriod;

            timeBar.timeAxis.start = newVal;

            endOfPeriod = angular.copy(newVal);
            endOfPeriod.setDate(1);
            // set time 23:59:59 to the last day of a previous month
            endOfPeriod.setHours(0, 0, -1, 0);
            endOfPeriod.setMonth(endOfPeriod.getMonth() + timeBar.timeAxis.months);

            timeBar.timeAxis.end = endOfPeriod;
        });
    });

    $scope.random = function() {
        var value = Math.floor((Math.random()*100)+1);
        var type;

        if (value < 25) {
            type = 'success';
        } else if (value < 50) {
            type = 'info';
        } else if (value < 75) {
            type = 'warning';
        } else {
            type = 'danger';
        }

        $scope.dynamic = value;
        $scope.dynamicObject = {
            value: value,
            type: type
        };
    };
    $scope.random();

    var types = ['success', 'info', 'warning', 'danger'];
    $scope.randomStacked = function() {
        $scope.stackedArray = [];
        $scope.stacked = [];

        var n = Math.floor((Math.random()*4)+1);

        for (var i=0; i < n; i++) {
            var value = Math.floor((Math.random()*30)+1);
            $scope.stackedArray.push(value);

            var index = Math.floor((Math.random()*4));
            $scope.stacked.push({
                value: value,
                type: types[index]
            });
        }
    };
    $scope.randomStacked();
}