angular.module('DemoApp', ['ui.bootstrap', 'timePeriodDirective',
    'directives/timePeriod/timePeriod.tpl.html']);

function DemoCtrl($scope, $locale) {

    $scope.datePicker = new Date();
    $scope.period = {
        start: angular.copy($scope.datePicker),
        months: 3
    };
};