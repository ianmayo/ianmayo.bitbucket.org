/**
 * @module timePeriodDirective
 */

angular.module('timePeriodDirective', ['ui.bootstrap'])

.constant('timePeriodConfig', {
    monthsPeriod: [3, 6, 12],
    monthsPeriodLabel: 'Months',
    dateFormat: 'yyyy-MM-dd'
})

.directive('timePeriod', ['timePeriodConfig', function (timePeriodConfig) {
    var setStartMonth = function (date) {
            // a month starts from first day
            date.setDate(1);
            // set time 00:00:00 to the first day of a month
            date.setHours(0, 0, 0, 0);
        },

        calculateEndPeriod = function (date, periodMonthsLength) {
            // a month starts from first day
            date.setDate(1);
            // set time 23:59:59 to the last day of a previous month
            date.setHours(0, 0, -1, 0);
            date.setMonth(date.getMonth() + periodMonthsLength);
        },

        movePeriod = function (date, step) {
            date.setMonth(date.getMonth() + step);
            return date;
        };

    return {
        restrict: 'C',
        replace: false,
        scope: {
            period: '=',
            isCollapsed: '='
        },
        templateUrl: 'directives/timePeriod/timePeriod.tpl.html',
        compile: function () {
            return function postLink(scope) {
                scope.periods = timePeriodConfig.monthsPeriod;

                scope.periodLabel = function (period) {
                    return period + ' ' + timePeriodConfig.monthsPeriodLabel;
                };

                scope.viewPeriod = scope.periodLabel(scope.period.months.toString());

                // move 1 month backward
                scope.prevPeriod = function () {
                    movePeriod(scope.period.start, -1);
                };

                // move 1 month forward
                scope.nextPeriod = function () {
                    movePeriod(scope.period.start, 1);
                };

                scope.selectPeriod = function (period) {
                    scope.period.months = parseInt(period, 10);
                };

                scope.$watch(function periodChanges() {
                    if (scope.isCollapsed) {
                        // work items are collapsed, skip renders
                        return false;
                    } else {
                        // render time period
                        return scope.period.start.getTime() + scope.period.months;
                    }

                }, function (periodChanged) {
                    if (!periodChanged) {
                        // it was probably collapsed, drop it out
                        return;
                    } else {
                        setStartMonth(scope.period.start);

                        scope.period.end = new Date(scope.period.start);
                        calculateEndPeriod(scope.period.end, scope.period.months);

                        scope.viewPeriod = scope.periodLabel(scope.period.months.toString());
                    }
                });
            };
        }
    };
}]);
