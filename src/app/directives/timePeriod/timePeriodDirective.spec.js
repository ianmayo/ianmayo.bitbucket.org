/**
 * timePeriod directive module's unit test spec
 */

describe('timePeriodDirective', function () {

    var scope, elm, $compile, dateParseService;

    beforeEach(module('directives/timePeriod/timePeriod.tpl.html'));
    beforeEach(module('timePeriodDirective'));
    beforeEach(module('dateParse'));

    beforeEach(inject(function(_$rootScope_, _$compile_, dateParse){
        $compile = _$compile_;
        scope = _$rootScope_;
        dateParseService = dateParse;
        scope.period = {
            start: dateParseService.fromISO('2013-05-22'),
            months: 3
        };
        scope.task = {isTaskCollapsed: false};

        elm = angular.element(
            '<div class="timePeriod" data-period="period"' +
            ' data-is-collapsed="task.isTaskCollapsed"></div>'
        );

        $compile(elm)(scope);
        scope.$digest();
    }));

    describe('DOM elements', function () {
        it('should have directive element', function () {
            expect(elm.hasClass('timePeriod')).toBeTruthy();
            expect(elm.attr('data-period')).toEqual('period');
        });
    });

    describe('Directive template', function () {
        var wrap, dropDownWrap, dropDown;

        it('should have period change elements', function () {
            expect(elm.children().children().length).toEqual(3);
        });

        it('should have period scroll elements', function () {
            wrap = elm.children();
            expect(wrap.children().eq(0).hasClass('btn')).toBeTruthy();
            expect(wrap.children().eq(2).hasClass('btn')).toBeTruthy();

        });

        it('should have period dropDown element', function () {
            dropDownWrap = elm.children().children().eq(1);
            expect(dropDownWrap.hasClass('btn-group')).toBeTruthy();
            expect(dropDownWrap.children().length).toEqual(2);
            expect(dropDownWrap.children().eq(0).hasClass('dropdown-toggle')).toBeTruthy();
            expect(dropDownWrap.children().eq(1).hasClass('dropdown-menu')).toBeTruthy();
            expect(dropDownWrap.children().eq(0).children().eq(0).hasClass('view-period')).toBeTruthy();
        });

        it('should have list periods', function () {
            dropDown = elm.children().children().eq(1).children().eq(1);
            expect(dropDown.children().length).toEqual(3);
        });
    });

    describe('Period dates', function () {
        it('should change start date to the first day of a month', function () {
            scope.period.start = dateParseService.fromISO('2013-05-05');
            scope.$digest();
            expect(scope.period.start).toEqual(dateParseService.fromISO('2013-05-01'));

            scope.period.start = dateParseService.fromISO('2013-05-22');
            scope.$digest();
            expect(scope.period.start).toEqual(dateParseService.fromISO('2013-05-01'));

            scope.period.start = dateParseService.fromISO('2013-05-01');
            scope.$digest();
            expect(scope.period.start).toEqual(dateParseService.fromISO('2013-05-01'));
        });

        /**
         * Depends on jQuery
         */
        it('should move period', function () {
            scope.period.start = dateParseService.fromISO('2013-05-01');

            // previous 3 months
            scope.period.months = 3;
            scope.$digest();
            elm.find('.btn').eq(0).click();
            expect(scope.period.start).toEqual(dateParseService.fromISO('2013-04-01'));

            // previous 6 months
            scope.period.months = 6;
            scope.$digest();
            elm.find('.btn').eq(0).click();
            expect(scope.period.start).toEqual(dateParseService.fromISO('2013-03-01'));

            // previous 12 months
            scope.period.months = 12;
            scope.$digest();
            elm.find('.btn').eq(0).click();
            expect(scope.period.start).toEqual(dateParseService.fromISO('2013-02-01'));

            scope.period.start = dateParseService.fromISO('2013-05-01');
            // next 3 months
            scope.period.months = 3;
            scope.$digest();
            elm.find('.btn').eq(2).click();
            expect(scope.period.start).toEqual(dateParseService.fromISO('2013-06-01'));

            // next 6 months
            scope.period.months = 6;
            scope.$digest();
            elm.find('.btn').eq(2).click();
            expect(scope.period.start).toEqual(dateParseService.fromISO('2013-07-01'));

            // next 12 months
            scope.period.months = 12;
            scope.$digest();
            elm.find('.btn').eq(2).click();
            expect(scope.period.start).toEqual(dateParseService.fromISO('2013-08-01'));
        });

        /**
         * Depends on jQuery
         */
        it('should move period', inject(function (timePeriodConfig) {
            var list = elm.find('li'),
                dropDown = elm.find('.view-period'),
                period;

            var label = function (period) {
                return period.toString() + ' ' + timePeriodConfig.monthsPeriodLabel;
            };

            period = 3;
            list.eq(0).click();
            expect(scope.period.months).toEqual(period);
            expect(dropDown.html()).toEqual(label(period));

            list.eq(1).click();
            period = 6;
            expect(scope.period.months).toEqual(period);
            expect(dropDown.html()).toEqual(label(period));

            list.eq(2).click();
            period = 12;
            expect(scope.period.months).toEqual(period);
            expect(dropDown.html()).toEqual(label(period));
        }));
    });

    describe('TimePeriod collapsing', function () {
        var htmlContent;

        it('should have collapsed attribute', function () {
            expect(elm.attr('data-is-collapsed')).toBeDefined();
        });

        it('should not change html content when work items are collapsed', function () {
            // get html content
            htmlContent = elm.html();

            // set task collapsed
            scope.task.isTaskCollapsed = true;

            // change start date and scale
            scope.period = {
                start: dateParseService.fromISO('2013-05-08'),
                months: 3
            };
            scope.$digest();
            expect(elm.html()).toEqual(htmlContent);
            expect(scope.period.start).toEqual(dateParseService.fromISO('2013-05-08'));

            // and change start date and scale
            scope.period = {
                start: dateParseService.fromISO('2013-05-16'),
                months: 6
            };
            scope.$digest();
            expect(elm.html()).toEqual(htmlContent);
            expect(scope.period.start).toEqual(dateParseService.fromISO('2013-05-16'));
        });

        it('should change html content when work items are expanded', function () {
            // get html content
            htmlContent = elm.html();

            // set task expanded
            scope.task.isTaskCollapsed = false;

            // change start date and scale
            scope.period = {
                start: dateParseService.fromISO('2013-05-12'),
                months: 6
            };
            scope.$digest();
            expect(elm.html()).not.toEqual(htmlContent);
            expect(scope.period.start).toEqual(dateParseService.fromISO('2013-05-01'));

            // get new html content
            htmlContent = elm.html();

            // and change start date and scale
            scope.period = {
                start: dateParseService.fromISO('2013-06-04'),
                months: 12
            };
            scope.$digest();
            expect(elm.html()).not.toEqual(htmlContent);
            expect(scope.period.start).toEqual(dateParseService.fromISO('2013-06-01'));
        });
        
        it('should change period "start" and "end" properties period changes', function () {
            var date = new Date();
            var periodMonthsLength;
            var periodStartDate;
            var periodEndDate;

            var setStartMonth = function (date) {
                    // a month starts from first day
                    date.setDate(1);
                    // set time 00:00:00 to the first day of a month
                    date.setHours(0, 0, 0, 0);
                },
                setEndMonth = function (date) {
                    // a month starts from first day
                    date.setDate(1);
                    // set time 23:59:59 to the last day of a previous month
                    date.setHours(0, 0, -1, 0);
                };

            scope.period.start = date;
            periodMonthsLength = 3;
            scope.period.months = periodMonthsLength;
            scope.$digest();
            expect(scope.period.end).toBeDefined();


            // test start period
            periodStartDate = new Date(date);
            setStartMonth(periodStartDate);
            expect(scope.period.start.getTime()).toEqual(periodStartDate.getTime());

            // test end period
            periodEndDate = new Date(date);
            setEndMonth(periodEndDate);
            periodEndDate.setMonth(periodEndDate.getMonth() + scope.period.months);
            expect(scope.period.end.getTime()).toEqual(periodEndDate.getTime());
        });
        
    });

});
