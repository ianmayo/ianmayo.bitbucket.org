angular.module('charsLimitDirective', [])

.directive('charsCounter', function () {
    return {
        restrict: 'AC',
        replace: false,
        scope: {
            modelText: "=",
            limit: '@'
        },
        template: '<span class="counter">{{count}}</span>',
        compile: function () {

            return function (scope) {
                scope.$watch('modelText', function (newVal, oldVal) {
                    if (newVal) {
                        if (newVal.length > scope.limit) {
                            scope.modelText = oldVal;
                        } else {
                            scope.count = scope.limit - newVal.length;
                        }
                    } else {
                        scope.count = scope.limit;
                    }
                });
            };
        }
    };
});
