/**
 * CharsLimit directive module's unit test spec
 */

describe('charsLimitDirective section', function () {

    var scope, elm, $compile;

    beforeEach(module('charsLimitDirective'));

    beforeEach(inject(function(_$rootScope_, _$compile_){
        $compile = _$compile_;
        scope = _$rootScope_;
        scope.counter = {
            model: '',
            limit: 10
        };

        elm = angular.element('<span data-model-text="counter.model" data-limit="{{counter.limit}}" class="charsCounter"></span>');

        $compile(elm)(scope);
        scope.$digest();
    }));

    describe('DOM elements', function () {
        it('should have class', function () {
            expect(elm.hasClass('charsCounter')).toBeTruthy();
        });

        it('should have "limit" attribute', function () {
            expect(parseInt(elm.attr('data-limit'), 10)).toBe(scope.counter.limit);
        });

        it('should have "model-count" attribute', function () {
            expect(elm.attr('data-model-text')).toBe('counter.model');
        });

        it('should have one children element', function () {
            expect(elm.children().length).toBe(1);
            expect(elm.children().hasClass('counter')).toBeTruthy();
        });
    });

    describe('change counter value', function () {
        it('init value', function () {
            expect(parseInt(elm.children().html(), 10)).toBe(scope.counter.limit);
        });

        it('should have one children element', function () {
            var str = 'string';
            scope.$apply(function () {
                scope.counter.model = str;
            });
            expect(parseInt(elm.children().html(), 10)).toBe(scope.counter.limit - str.length);
        });

        it('counter should have initial number if text is too long from start', function () {
            var str = 'long string';
            scope.$apply(function () {
                scope.counter.model = str;
            });
            expect(parseInt(elm.children().html(), 10)).toBe(scope.counter.limit);
        });

        it('counter should prevent entering too long text', function () {
            var strOld = 'string', strNew = 'very long string';

            scope.$apply(function () {
                scope.counter.model = strOld;
            });
            expect(parseInt(elm.children().html(), 10)).toBe(scope.counter.limit - strOld.length);

            scope.$apply(function () {
                scope.counter.model = strNew;
            });
            expect(parseInt(elm.children().html(), 10)).toBe(scope.counter.limit - strOld.length);
        });
    });
});
