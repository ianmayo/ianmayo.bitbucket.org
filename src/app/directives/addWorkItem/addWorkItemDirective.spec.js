/**
 * Add Work Item directive module's unit test spec
 */

describe('Add Work Item Directive', function () {

    var $scope, $elm, $compile;

    beforeEach(module('addWorkItemDirective'));

    beforeEach(inject(function(_$rootScope_, _$compile_) {
        $scope = _$rootScope_;
        $compile = _$compile_;

        $scope.task = {
            items: []
        };

        $elm = angular.element(
            '<span class="addWorkItem" data-items="task.items"></span>');

        $compile($elm)($scope);
        $scope.$digest();
    }));

    describe('DOM elements', function () {
        it('should have work items attribute', function () {
            expect($elm.attr('data-items')).toBeDefined();
            expect($elm.attr('data-items')).toEqual('task.items');
        });
    });

    describe('Work items collection', function () {
        it('should have empty work items list', function () {
            expect($scope.task.items.length).toEqual(0);
        });

        it('should have non empty work items list', function () {
            $elm.click();
            expect($scope.task.items.length).toEqual(1);
        });
    });
});
