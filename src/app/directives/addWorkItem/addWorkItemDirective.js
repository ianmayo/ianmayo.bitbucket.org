/**
 * @module Add Work Item directive
 */

angular.module('addWorkItemDirective', [])

.value('addWorkItemConfig', {
    defaultWorkItem: {
        title: '',
        startDate: new Date(),
        finishDate: new Date(),
        percentComplete: 0,
        assigned: [],
        description: '',
        isDeliverable: false,
        isExpenditure: false,
        isPublication: false,
        isTrial: false
    }
})

.directive('addWorkItem', function (addWorkItemConfig) {
    return {
        restrict: 'C',
        replace: false,
        scope: {
            items: "="
        },
        link: function (scope, elm) {
            elm.bind('click', function addWorkItemHandler () {
                scope.$apply(function addWorkItemDigest () {
                    scope.items.push(
                        angular.extend({}, addWorkItemConfig.defaultWorkItem)
                    );
                });
            });
        }
    };
});
