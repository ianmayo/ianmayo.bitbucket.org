/**
 * @module ViewTask
 */
angular.module('taskApp.viewTask', [
    'ui.state',
    'placeholders',
    'ui.bootstrap',
    'tasks',
    'titleService',
    'activityWindowDirective',
    'timePeriodDirective',
    'timeBarAxisDirective',
    'timeBarDirective',
    'addWorkItemDirective',
    'workItemTypeDirective',
    'guidanceDirective',
    'tooltipWatchDirective',
    'xeditable',
    'restMetadata'
])

/**
 * @module ViewTask
 * @class Route
 */
.config(function config ($stateProvider) {

    $stateProvider.state('task', {
        url: '/task/:id',
        views: {
            main: {
                controller: 'ViewTaskCtrl',
                templateUrl: 'viewTask/viewTask.tpl.html'
            }
        },
        resolve: {
            theTask: ['tasks', '$stateParams', function (tasks, $stateParams) {
                return tasks.getTaskById($stateParams.id).then(function (task) {
                    return task;
                });
            }],

            metadataApi: ['restMetadata', function (restMetadata) {
                return restMetadata.then(function (api) {
                    return api;
                });
            }]
        }
    });
})

/**
 * @module ViewTask
 * @class ViewTaskCtrl (controller)
 */
.controller('ViewTaskCtrl',
    function ViewTaskController ($scope, $state, titleService, tasks, metadata, guidanceConfig, addWorkItemConfig,
         theTask, metadataApi) {

    /**
     *  Set default values for a new work item.
     *  We set custom values because they could be overridden in the AddTask module.
     */
    angular.extend(addWorkItemConfig.defaultWorkItem, {
        name: 'Item title',
        description: 'Item description'
    });

    titleService.setTitle('View Task');

    guidanceConfig.addIcon = function (field, tooltip) {
        field.next().after(tooltip);
    };

    /**
     * Initial activity type filtering
     * @default "report"
     */
    $scope.activityFilterType = ['status'];

    $scope.task =  theTask;

    /**
     * Available data for select boxes of Add task form
     */
    $scope.availablePriorities = metadataApi.getPriorities();
    $scope.availableDivisions = metadataApi.getDivisions();
    $scope.availableOrganisations = metadataApi.getCustomers();
    $scope.availableCategories = metadataApi.getCategories();
    $scope.availableLeaders = metadataApi.getUsers();

    //  TimePeriod directive
    $scope.period = {
        start: new Date(),
        months: 3
    };

    /**
     * This is needed for the activity items filtering to show them in the activity list.
     * @param {Object} activity item
     * @returns {boolean}
     */
    $scope.filterActivityType = function (activity) {
        return _.contains($scope.activityFilterType, activity.type);
    };

    /**
     * The method defines state of an activity filter
     * @param {String} type
     * @returns {Boolean}
     */
    $scope.isActiveType = function (type) {
        return _.contains($scope.activityFilterType, type);
    };

    /**
     * The method changes state of an activity filter
     * @param {String} type
     */
    $scope.resolveActivityType = function (type) {
        if(_.contains($scope.activityFilterType, type)) {
            $scope.activityFilterType = _.without($scope.activityFilterType, type);
        } else {
            $scope.activityFilterType.push(type);
        }
    };

    /**
     * Add report window
     * @param {String} type
     * @param {Object} task
     */
    $scope.openActivityWindow = function (type, task) {
        $scope.$broadcast('showActivityWindow', {type: type, task: task});
    };

    $scope.updateTask = function (newValue, fieldName) {
        return tasks.change($scope.task.taskId, fieldName, newValue);
    };

    $scope.showTaskCategories = function() {
        var selected = [];
        angular.forEach($scope.availableCategories, function (category) {
            if (_.indexOf($scope.task.categories, category.id) >= 0) {
                selected.push(category.value);
            }
        });
        return selected.length ? selected.join(', ') : 'Not set';
    };

    $scope.showTaskLeader = function() {
        var selected = {};
        selected = _.find($scope.availableLeaders, function (leader) {
            return leader.id === $scope.task.leader.id;
        });
        return selected.value || 'Not set';
    };

    $scope.showWorkItemPeople = function (workItemPeople) {
        var selected = [];
        angular.forEach($scope.availableLeaders, function (leader) {
            if (_.indexOf(workItemPeople, leader.id) >= 0) {
                selected.push(leader.value);
            }
        });
        return selected.length ? selected.join(', ') : 'Not set';
    };

    $scope.showTaskPriority = function () {
        var selected = {};
        selected = _.find($scope.availablePriorities, function (priority) {
            return priority.id === $scope.task.priority.id;
        });
        return selected.value || 'Not set';
    };

    $scope.showTaskDivision = function () {
        var selected = {};
        selected = _.find($scope.availableDivisions, function (division) {
            return division.id === $scope.task.division.id;
        });
        return selected.value || 'Not set';
    };

    $scope.showCustomerOrganisation = function () {
        var selected = {};
        selected = _.find($scope.availableOrganisations, function (org) {
            return org.id === $scope.task.customerOrganisationId;
        });
        return selected.value || 'Not set';
    };

    $scope.updateWorkItem = function (newValue, fieldName) {
        var requiredProperties = ['name'];

        if (_.contains(requiredProperties, fieldName) && !newValue) {
            return "The field is required";
        }
    };
});
