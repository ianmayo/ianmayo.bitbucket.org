/**
 * This is the e2e scenario of ViewTask module
 *
 * Notice for future we definitely should have several users with static data for the tests
 * Also we should notice that we should avoid real AJAX calls in e2e to make it as fast as it can be it means
 * that we have to switch on our future data services to the mock JSON data when we run e2e
 */

describe('ViewTask module', function() {
    var taskUrl = '/task/1',
        url = '/base/build/index.html#' + taskUrl;

    var timePeriodSelector = '.task-form .timePeriod';
    var timePeriodLabelSelector = timePeriodSelector + ' .view-period';
    var timePeriodToggleSelector = timePeriodSelector + ' .dropdown-toggle';
    var timePeriodMenuSelector = timePeriodSelector + ' .dropdown-menu';

    var timeBarAxisSelector = '.task-form .timeBarAxis';
    var timeBarAxisColumnsSelector = timeBarAxisSelector + ' > .axisColumn';

    var workItemsTableSelector = '.task-form .workItems';
    var timeBarSelector = workItemsTableSelector + ' .timeBar:first';
    var timeBarCanvasSelector = workItemsTableSelector + ' .canvas:first';
    var timeBarProgressWrapSelector = timeBarSelector + ' > .startDateOffset';
    var itemStartDateCellSelector = workItemsTableSelector + ' tr:eq(1) > td:eq(5) a';
    var itemFinishDateCellSelector = workItemsTableSelector + ' tr:eq(1) > td:eq(6) a';

    var addWorkItemBtnSelector = workItemsTableSelector + ' + .tfoot .addWorkItem';

    var addCommentSelector = '.btnAddComment';
    var activityWindowSelector = '.activityWindow';
    var addCommentFormSelector = '.formAddComment';
    var activityItemsSelector = '.activity-items';
    var activityActionsSelector = '.activity-actions';

    var addStatusReportBtnSelector =  '.btnStatusReport';
    var statusReportFormSelector =  '.formStatusReport';

    beforeEach(function () {
        browser().navigateTo(url);
    });

    describe('Initial', function() {
        it('initial state', function() {
            expect(browser().window().hash()).toEqual(taskUrl);
        });
    });

    describe('Add status report window', function () {
        var initialStatus = 'suspended', changeStatus = 'complete';

        // TODO: It was added to prevent the starting of the tests before DOM is fully loaded please remove it in future
        it('DOM waiter', function () {
            sleep(1);
        });

        it('should have status title on a page', function () {
            // TODO uncomment it when mock task fixed
//            expect(element('.title-container .status').html()).toEqual(initialStatus);
        });

        it('should have directive element', function () {
            // check element with directive
            expect(element(activityWindowSelector).count()).toEqual(1);
        });

        it('should have one button on page', function () {
            expect(element(addStatusReportBtnSelector).count()).toEqual(1);
        });

        it('should have window directive', function () {
            // check element with directive
            expect(element(statusReportFormSelector).count()).toEqual(0);
        });

        it('window should have "Close" button', function () {
            // open modal window
            element(addStatusReportBtnSelector).click();
            sleep(1);
            expect(element(statusReportFormSelector).count()).toEqual(1);

            // close window
            element(statusReportFormSelector + ' .btnClose').click();
            sleep(1);
            expect(element(statusReportFormSelector).count()).toEqual(0);
        });

        it('window should have "Change" button', function () {
            // open modal window
            element(addStatusReportBtnSelector).click();
            sleep(1);
            expect(element(statusReportFormSelector).count()).toEqual(1);

            // change status
            select('task.status').option(changeStatus);
            sleep(1);

            // change and close window
            element(statusReportFormSelector + ' .btnChange').click();
            sleep(1);
            expect(element(statusReportFormSelector).count()).toEqual(0);

            // check status
            expect(element('.title-container .status').html()).toEqual(changeStatus);
        });
    });

    describe('Focused field in the Add Status Report window', function () {
        it('should open a popup dialog and set a focus on the "comment" field', function () {
            // open modal window
            element(addStatusReportBtnSelector + ' :eq(0)').click();
            sleep(3);

            // check is element on focus
            expect(element(statusReportFormSelector + ' textarea:focus').count()).toBe(1);

            // close window
            element(statusReportFormSelector +' .btnClose').click();
            sleep(1);
        });
    });

    describe('Add comment window', function () {
        it('should have a button to open modal window', function () {
            expect(element(addCommentSelector).count()).toEqual(1);
        });

        it('should have directive element', function () {
            // check element with directive
            expect(element(activityWindowSelector).count()).toEqual(1);
        });

        it('should not have Add Comment window', function () {
            // check element with directive
            expect(element(addCommentFormSelector).count()).toEqual(0);
        });

        it('window should have "Close" button', function () {
            // open modal window
            element(addCommentSelector).click();
            expect(element(addCommentFormSelector).count()).toEqual(1);

            // close window
            element(addCommentFormSelector + ' .btnClose').click();
            expect(element(addCommentFormSelector).count()).toEqual(0);
        });

        it('window should have "Add comment" button', function () {
            var mockCommentText = 'Add comment text';
            // hide Status Report activities list
            element(activityActionsSelector + ' .btn-group button:eq(0)').click();
            // show Comment activities list
            element(activityActionsSelector + ' .btn-group button:eq(1)').click();
            sleep(1);

            // open modal window
            element(addCommentSelector).click();
            sleep(1);
            expect(element(addCommentFormSelector).count()).toEqual(1);

            // add a comment
            input('report.text').enter(mockCommentText);
            sleep(1);

            // close window
            element(addCommentFormSelector + ' .btnChange').click();
            sleep(1);
            expect(element(addCommentFormSelector).count()).toEqual(0);

            // check the comment. It should be the first because events are ordered by reverse date
            sleep(1);
//            expect(element(activityItemsSelector + ' li p').text()).toContain(mockCommentText);
        });
    });

    describe('Focused field in the Add Comment window', function () {

        // TODO: It was added to prevent the starting of the tests before DOM is fully loaded please remove it in future
        it('DOM waiter', function () {
            sleep(1);
        });

        it('should open a popup dialog and set a focus on the "comment" field', function () {
            // open modal window
            element(addCommentSelector).click();
            sleep(3);

            // check is element on focus
            expect(element(addCommentFormSelector + ' textarea:focus').count()).toBe(1);

            // close window
            element(addCommentFormSelector + ' .btnChange').click();
            sleep(1);
        });
    });

    describe('TimeBarAxis module', function () {

//        it('should have workItems element', function () {
//            expect(element('.workItems').count()).toEqual(1);
//            expect(element('.workItems tbody').count()).toEqual(1);
//        });
//
//        it('should have timeBarAxis element', function () {
//            expect(element('.timeBarAxis').count()).toEqual(1);
//        });
//
//        it('should have axisColumn elements', function () {
//            // 3, 6 or 12 month
//            expect(element('.axisColumn').count()).toBeGreaterThan(2);
//        });
    });

    describe('Time period select', function () {
//        it('should have a period select option', function () {
//            expect(element(timePeriodSelector).count()).toEqual(1);
//            // open / close element
//            expect(element(timePeriodToggleSelector).count()).toEqual(1);
//            // month list
//            expect(element(timePeriodMenuSelector).count()).toEqual(1);
//        });
//
//        it('should change months period select', function () {
//            // select first option - 3 month period
//            element(timePeriodMenuSelector + ' > li:eq(0)').click();
//            sleep(1);
//            expect(element(timePeriodLabelSelector).html()).toEqual('3 Months');
//
//            // select second option - 6 month period
//            element(timePeriodMenuSelector + ' > li:eq(1)').click();
//            sleep(1);
//            expect(element(timePeriodLabelSelector).html()).toEqual('6 Months');
//
//            // select second option - 12 month period
//            element(timePeriodMenuSelector + ' > li:eq(2)').click();
//            sleep(1);
//            expect(element(timePeriodLabelSelector).html()).toEqual('12 Months');
//        });
    });

    describe('Time axis bar', function () {
        var periodCheck = function(originDate) {
            // select 3 month period
            element(timePeriodMenuSelector + ' > li:eq(0)').click();
            sleep(1);
            expect(element(timeBarAxisColumnsSelector).count()).toEqual(3);
            // a month label
            expect(element(timeBarAxisColumnsSelector + ':first > .columnMonth').html()).toEqual(originDate);

            // select 6 month period
            element(timePeriodMenuSelector + ' > li:eq(1)').click();
            sleep(1);
            expect(element(timeBarAxisColumnsSelector).count()).toEqual(6);
            // a month label
            expect(element(timeBarAxisColumnsSelector + ':first > .columnMonth').html()).toEqual(originDate);

            // select 12 month period
            element(timePeriodMenuSelector + ' > li:eq(2)').click();
            sleep(1);
            expect(element(timeBarAxisColumnsSelector).count()).toEqual(12);
            // a month label
            expect(element(timeBarAxisColumnsSelector + ':first > .columnMonth').html()).toEqual(originDate);
        };

//        it('should have a time axis bar', function () {
//            expect(element(timeBarAxisSelector).count()).toEqual(1);
//            // axis columns
//            expect(element(timeBarAxisColumnsSelector).count()).toBeGreaterThan(1);
//            // a month column
//            expect(element(timeBarAxisColumnsSelector + ':first > .columnMonth').count()).toEqual(1);
//        });
//
//        it('should change month columns on a page when time period changes', function () {
//            var $filter = angular.injector(['ng']).get('$filter');
//            var curMonth = $filter('date')(new Date(), 'MMM');
//
//            periodCheck(curMonth);
//        });
//
//        it('should change month columns on a page when time period origin date changes', function () {
//            var $filter = angular.injector(['ng']).get('$filter');
//            var curDate = new Date();
//            var prev1Month = $filter('date')(curDate.setMonth(curDate.getMonth() - 1), 'MMM');
//
//            // click on the prev period button
//            element(timePeriodSelector + ' .btn:first').click();
//            sleep(1);
//
//            periodCheck(prev1Month);
//        });

        describe('Time bar module', function () {
            var timeBar;
            var testDirection;

            var timeBarVisibility = function(direction) {
                var dateParseService = angular.injector(['dateParse']).get('dateParse');
                var itemFinishDate;
                var itemStartDate;
                var periodEndDate;
                var periodStartDate;


                var itemDates = function () {
                    var dateFromString = function (dateString) {
                        // trim a string
                        dateString = dateString.replace(/^\s*/, '').replace(/\s*$/, '');

                        return dateParseService.fromISO(dateString);
                    };

                    // define start date for time bars canvas
                    periodStartDate = new Date();
                    // a month starts from 1 day
                    periodStartDate.setDate(1);

                    // set period end date (3 month)
                    periodEndDate = new Date(periodStartDate);
                    periodEndDate.setMonth(periodEndDate.getMonth() + 3);

                    element(itemStartDateCellSelector).query(function (startDate, done) {
                        itemStartDate = dateFromString(startDate.data('iso-date'));
                        done();
                    });

                    element(itemFinishDateCellSelector).query(function (finishDate, done) {
                        itemFinishDate = dateFromString(finishDate.data('iso-date'));
                        done();
                    });
                };

                var moveToItemForward = function () {
                    var changePeriodDirectionSel = timePeriodSelector + ' .btn:last';

                    element(changePeriodDirectionSel).query(function (btn, done) {
                        // change period
                        btn.click();
                        sleep(0.5);

                        periodStartDate.setMonth(periodStartDate.getMonth() + 1);
                        periodEndDate.setMonth(periodEndDate.getMonth() + 1);

                        if (periodStartDate.getTime() < itemFinishDate.getTime() &&
                                itemFinishDate.getTime() < periodEndDate.getTime()) {
                            // right edge (item finish date) is visible
                            isItemVisible();
                            moveToItemForward();
                        } else if (periodStartDate.getTime() < itemStartDate.getTime() &&
                                periodEndDate.getTime() > itemStartDate.getTime()) {
                            // left edge (item start date) is visible
                            isItemVisible();
                        } else if (periodEndDate.getTime() < itemStartDate.getTime()) {
                            // finally, item is hidden
                            isItemInvisible();

                            // click to previous period and the item should be visible
                            element(timePeriodSelector + ' .btn:first').click();
                            sleep(0.5);
                            isItemVisible();
                        } else if (itemFinishDate.getTime() < periodEndDate.getTime()) {
                            // an item is behind of canvas, there is no sense click forward
                            done();
                        } else {
                            // continue changing period, probably the item should be there
                            moveToItemForward();
                        }

                        done();
                    });
                };

                var moveToItemBackward = function () {
                    var changePeriodDirectionSel = timePeriodSelector + ' .btn:first';

                    element(changePeriodDirectionSel).query(function (btn, done) {
                        // change period
                        btn.click();
                        sleep(0.5);

                        periodStartDate.setMonth(periodStartDate.getMonth() - 1);
                        periodEndDate.setMonth(periodEndDate.getMonth() - 1);

                        if (periodStartDate.getTime() < itemFinishDate.getTime() &&
                                itemFinishDate.getTime() < periodEndDate.getTime()) {
                            // right edge (item finish date) is visible
                            isItemVisible();
                            moveToItemBackward();
                        } else if (periodStartDate.getTime() < itemStartDate.getTime() &&
                                periodEndDate.getTime() > itemStartDate.getTime()) {
                            // left edge (item start date) is visible
                            isItemVisible();
                            moveToItemBackward();
                        } else if (periodEndDate.getTime() < itemStartDate.getTime()) {
                            // finally, item is hidden
                            isItemInvisible();

                            // move to next period and the item should be visible
                            element(timePeriodSelector + ' .btn:last').click();
                            sleep(0.5);
                            isItemVisible();

                        } else if (periodEndDate.getTime() < itemStartDate.getTime()) {
                            // an item is ahead of canvas , there is no sense click backward
                            done();
                        } else {
                            // continue changing period, probably the item should be there
                            moveToItemBackward();
                        }

                        done();
                    });
                };

                var isItemVisible = function () {
                    sleep(0.5);
                    expect(element(timeBarProgressWrapSelector).css('visibility')).toEqual('visible');
                };

                var isItemInvisible = function () {
                    sleep(0.5);
                    expect(element(timeBarProgressWrapSelector).css('visibility')).toEqual('hidden');
                };

                itemDates();

                if ('forward' === direction) {
                    moveToItemForward();
                }

                if ('backward' === direction) {
                    moveToItemBackward();
                }
            };

//            it('should have at least one time bar element', function () {
//                // work items table
//                expect(element(workItemsTableSelector).count()).toEqual(1);
//
//                // time bar directive element
//                timeBar = element(timeBarSelector);
//                expect(timeBar.count()).toEqual(1);
//
//                // canvas for time bar
//                expect(element(timeBarCanvasSelector).count()).toEqual(1);
//
//                // container for progress bar
//                expect(element(timeBarProgressWrapSelector).count()).toEqual(1);
//
//                // time bar percent completion
//                expect(timeBar.attr('data-complete-percent')).toEqual('workItem.percent');
//                // time bar item data
//                expect(timeBar.attr('data-item')).toEqual('workItem');
//                // time period start and time axis scale
//                expect(timeBar.attr('data-time-axis')).toEqual('period');
//            });

//            it('should test item visibility in the past', function () {
//                // select 3 month period
//                element(timePeriodMenuSelector + ' > li:eq(0)').click();
//                sleep(1);
//
//                expect(element(itemStartDateCellSelector).count()).toEqual(1);
//                expect(element(itemFinishDateCellSelector).count()).toEqual(1);
//                testDirection = 'backward';
//                timeBarVisibility(testDirection);
//            });
//
//            it('should test item visibility in the future', function () {
//                // select 3 month period
//                element(timePeriodMenuSelector + ' > li:eq(0)').click();
//                sleep(1);
//
//                expect(element(itemStartDateCellSelector).count()).toEqual(1);
//                expect(element(itemFinishDateCellSelector).count()).toEqual(1);
//                testDirection = 'forward';
//                timeBarVisibility(testDirection);
//            });
        });

        describe('addWorkItem module', function () {
            var workItemsSelector = workItemsTableSelector + ' tbody tr';

//            it('should have work items element', function () {
//                // work items table
//                expect(element(workItemsTableSelector).count()).toEqual(1);
//                // add work item button
//                expect(element(addWorkItemBtnSelector).count()).toEqual(1);
//            });
//
//            it('should add new a work item to existed list', function () {
//                var testItemCount = function (itemsCount) {
//                    // click the New Work Item button
//                    element(addWorkItemBtnSelector).click();
//                    sleep(1);
//
//                    // it should add one item
//                    expect(element(workItemsSelector).count()).toEqual(itemsCount + 1);
//                };
//
//                element(workItemsSelector).query(function (items, done) {
//                    var initialItemsCount = items.length;
//                    testItemCount(initialItemsCount);
//                    done();
//                });
//            });
        });

        describe('workItemType module', function () {
            var workItemSelector = workItemsTableSelector + ' tbody tr:first';
            var workItemTypeSelector = workItemSelector + ' .workItemType';
            var typeSelector = workItemTypeSelector + ' a';

            var testType = function (selector) {
                expect(element(selector).attr('class')).toContain('active');
                element(selector).click();
                sleep(1);
                expect(element(selector).attr('class')).not().toContain('active');
            };

            var setTypeActive = function (selector) {
                element(selector).query(function (type, done) {
                    if (!type.hasClass('active')) {
                        type.click();
                        sleep(1);
                    }
                    testType(selector);

                    done();
                });
            };

//            it('should have work item element', function () {
//                // check if work items table exists
//                expect(element(workItemsTableSelector).count()).toEqual(1);
//                // check if work item row exists
//                expect(element(workItemSelector).count()).toEqual(1);
//                // check if work item types directive exists
//                expect(element(workItemTypeSelector).count()).toEqual(1);
//                // check if work item types directive exists
//                expect(element(typeSelector).count()).toEqual(4);
//            });
//
//            it('should change "Deliverable" type', function () {
//                var selector = typeSelector + '[title="Deliverable"]';
//                setTypeActive(selector);
//            });
//
//            it('should change "isTrial" type', function () {
//                var selector = typeSelector + '[title="Trial"]';
//                setTypeActive(selector);
//            });
//
//            it('should change "isPublication" type', function () {
//                var selector = typeSelector + '[title="Publication"]';
//                setTypeActive(selector);
//            });
//
//            it('should change "isFinancial" type', function () {
//                var selector = typeSelector + '[title="Finance"]';
//                setTypeActive(selector);
//            });
        });
    });

//    describe('Activity filter buttons', function () {
//        it('window should have type activity buttons', function () {
//            // 3 type button Status Report, Comment, Audit log
//            expect(element(activityActionsSelector + ' .btn-group button').count()).toEqual(3);
//        });
//
//        it('window should have checked "Status Report" activities by default', function () {
//            var statusReportButton;
//            // By Status Report button should be active
//            statusReportButton = element(activityActionsSelector + ' .btn-group button:eq(0).active');
//            expect(statusReportButton.count()).toEqual(1);
//            expect(statusReportButton.html()).toEqual('Status Reports');
//        });
//
//        it('window should change activity filters', function () {
//            // add Comments to the filter
//            element(activityActionsSelector + ' .btn-group button:eq(1)').click();
//            sleep(1);
//            expect(element(activityActionsSelector + ' .btn-group button.active').count()).toEqual(2);
//
//            // add Audit Log to the filter
//            element(activityActionsSelector + ' .btn-group button:eq(2)').click();
//            sleep(1);
//            expect(element(activityActionsSelector + ' .btn-group button.active').count()).toEqual(3);
//
//            // remove Status Report, Comments, Audit Log filters
//            element(activityActionsSelector + ' .btn-group button:eq(0)').click();
//            element(activityActionsSelector + ' .btn-group button:eq(1)').click();
//            element(activityActionsSelector + ' .btn-group button:eq(2)').click();
//            sleep(1);
//            expect(element(activityActionsSelector + ' .btn-group button.active').count()).toEqual(0);
//        });
//    });

//    describe('Activity items list', function () {
//        var activityListItemsSelector = activityItemsSelector + ' li';
//        it('window should have "Status Report" items by default', function () {
//            expect(element(activityListItemsSelector).count()).toEqual(5);
//        });
//
//        it('window should change activity items', function () {
//            // remove Status Report items
//            element(activityActionsSelector + ' .btn-group button:eq(0)').click();
//            // add Comments to the filter
//            element(activityActionsSelector + ' .btn-group button:eq(1)').click();
//            sleep(1);
//            expect(element(activityListItemsSelector).count()).toEqual(2);
//
//            // add Status Report items (Status Report + Comments)
//            element(activityActionsSelector + ' .btn-group button:eq(0)').click();
//            sleep(1);
//            expect(element(activityListItemsSelector).count()).toEqual(7);
//
//            // add Audit Log to the filter
//            element(activityActionsSelector + ' .btn-group button:eq(2)').click();
//            sleep(1);
//            // add Audit Log items (Status Report + Comments + Audit Log)
//            expect(element(activityListItemsSelector).count()).toEqual(7);
//
//            // remove Status Report, Comments, Audit Log filters
//            element(activityActionsSelector + ' .btn-group button:eq(0)').click();
//            element(activityActionsSelector + ' .btn-group button:eq(1)').click();
//            element(activityActionsSelector + ' .btn-group button:eq(2)').click();
//            sleep(1);
//            expect(element(activityActionsSelector + ' .btn-group button.active').count()).toEqual(0);
//            expect(element(activityListItemsSelector).count()).toEqual(0);
//        });
//    });
});
