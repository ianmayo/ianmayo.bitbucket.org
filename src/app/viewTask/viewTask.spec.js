/**
 * View Task module's unit test spec
 */

describe('View Task section', function() {
    var $scope, tasks, metadata;

    beforeEach(module('taskApp.viewTask'));
    beforeEach(module('mockTask'));
    beforeEach(module('mockMetadata'));

    describe('ViewTaskCtrl', function () {
        var mockTask = {
                id: 1,
                title: 'Task title',
                status: 'scoped'
            };

        beforeEach(inject(function ($rootScope, $controller, $state, _mockTask_, _mockMetadata_) {
            tasks = _mockTask_;
            metadata = _mockMetadata_;

            $scope = $rootScope.$new();
            $state.params.id = 1;

            $controller('ViewTaskCtrl', {
                $scope: $scope,
                theTask: tasks.getTasks()[1],
                metadataApi: metadata
            });
        }));

        describe('Task activity window', function () {
            it('should process status report window', function () {
                expect(_.isFunction($scope.openActivityWindow)).toBeTruthy();

                spyOn($scope, '$broadcast');
                $scope.openActivityWindow('report', mockTask);
                expect($scope.$broadcast).toHaveBeenCalledWith('showActivityWindow',
                    {type: 'report', task: mockTask}
                );
            });

            it('should process add comment window', function () {
                expect(_.isFunction($scope.openActivityWindow)).toBeTruthy();

                spyOn($scope, '$broadcast');
                $scope.openActivityWindow('comment', mockTask);
                expect($scope.$broadcast).toHaveBeenCalledWith('showActivityWindow',
                    {type: 'comment', task: mockTask}
                );
            });
        });

        describe('TimeBarAxis module', function () {

            it('should have items list', inject(function () {
                expect(_.isObject($scope.task.items)).toBeTruthy();
            }));

            it('should have timeAxisPeriod settings', function () {
                expect(_.isObject($scope.period)).toBeTruthy();
            });

            it('should task items data', function () {
                expect(angular.isDate($scope.task.items[0].start)).toBeTruthy();
                expect(angular.isDate($scope.task.items[0].finish)).toBeTruthy();
            });
        });

        describe('TimePeriod module', function () {
            it('should have time period object', function () {
                expect(_.isObject($scope.period)).toBeTruthy();
                expect(_.isNumber($scope.period.months)).toBeTruthy();
                // Current date object
                expect(_.isObject($scope.period.start)).toBeTruthy();
            });
        });

        describe('addWorkItem module', function () {
            it('should have work items list in a task', function () {
                expect(_.isObject($scope.task)).toBeTruthy();
                expect(_.isArray($scope.task.items)).toBeTruthy();
            });
        });
    });
});
