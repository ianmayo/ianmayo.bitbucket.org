/**
 * Task Tracker application
 */

angular.module('taskApp', [
    'templates-app',
    'templates-common',
    'ui.route',
    'taskApp.welcome',
    'taskApp.navbar',
    'taskApp.main',
    'taskApp.addTask',
    'taskApp.viewTask'
])

.config(function myAppConfig ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/welcome');
})

.run(function run (titleService, editableOptions, editableThemes) {
    titleService.setSuffix(' | Task Tracker');

    // remove 'controls' class from template
    editableThemes['bs2'].controlsTpl = '<div class="editable-controls control-group" ng-class="{\'error\': $error}"></div>';
    // disable html5 validation in xeditable forms
    editableThemes['bs2'].formTpl = '<form class="form-inline editable-wrap" role="form" novalidate></form>';
    // xeditable module bootstrap2 theme. Can be also 'bs3', 'default'
    editableOptions.theme = 'bs2';
    // do nothing when control losses focus. Values: `cancel|submit|ignore`.
    editableOptions.blur = 'ignore';

})

.controller('AppCtrl', function AppCtrl ($scope, $location) {
    // do nothing
});
