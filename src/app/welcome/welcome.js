/**
 * @module Welcome
 */

angular.module('taskApp.welcome', [
    'titleService',
    'placeholders',
    'ui.route',
    'ui.state'
])

/**
 * @module Welcome
 * @class Route
 */

.config(function config ($stateProvider) {

    $stateProvider.state('welcome', {
        url: '/welcome',
        views: {
            main: {
                controller: 'WelcomeCtrl',
                templateUrl: 'welcome/welcome.tpl.html'
            }
        }
    });
})

/**
 * @module Welcome
 * @class WelcomeCtrl (controller)
 */

.controller('WelcomeCtrl', function WelcomeCtrl ($scope, titleService) {

    titleService.setTitle('Welcome');
});
