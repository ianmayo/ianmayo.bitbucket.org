/**
 * Welcome module's specs
 */

describe('Welcome section', function () {
    var $scope;

    beforeEach(module('taskApp.welcome'));

    beforeEach(inject(function ($rootScope, $controller) {

        $scope = $rootScope.$new();

        $controller('WelcomeCtrl', {
            $scope: $scope
        });
    }));

    describe('WelcomeCtrl', function () {

        it('should exist', function () {
            expect($scope).toBeDefined();
        });
    });
});
