describe('dateParse', function () {

    var dateParse;

    var stubDateTime = '2013-06-01 13:20:15';
    var stubDateISO = '2013-06-01';
    var stubDateTimeISO = '2013-06-01T10:30:12';

    beforeEach(module('dateParse'));

    beforeEach(inject(function ($injector) {
        dateParse = $injector.get('dateParse');
    }));

    describe('fromISO', function () {
        it('should return Date object if a string in ISO format', function () {
            expect(_.isDate(dateParse.fromISO(stubDateISO))).toBeTruthy();
            expect(_.isDate(dateParse.fromISO(stubDateTimeISO))).toBeTruthy();
        });

        it('should return undefined if a sting not in ISO format', function () {
            expect(dateParse.fromISO(stubDateTime)).toBeUndefined();
        });
    });
});
