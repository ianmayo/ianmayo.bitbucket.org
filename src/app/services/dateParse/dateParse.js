/**
 * @module dateParse
 */

angular.module('dateParse', [])

/**
 * @module dateParse
 * @class Service
 * @description parse date from ISO string
 */

.service('dateParse', function () {

    var ISODateRegex = /^(\d{4})-(\d{2})-(\d{2})$/;
    var ISODateTimeRegex = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})$/;
    return {

        /**
         * Returns Date object if a date string has ISO format
         * @param dateString
         * @returns {{}}
         */
        fromISO: function (dateString) {
            var date;
            var struct = ISODateTimeRegex.exec(dateString) || ISODateRegex.exec(dateString);
            if (struct) {
                date = new Date(
                    parseInt(struct[1], 10),
                    parseInt(struct[2], 10) - 1,
                    parseInt(struct[3], 10),
                    (parseInt(struct[4], 10) || 0),
                    (parseInt(struct[5], 10) || 0),
                    (parseInt(struct[6], 10) || 0)
                );
            }
            return date;
        }
    };
});
