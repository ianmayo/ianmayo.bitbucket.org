angular.module('metadata', [])

.service('metadata', function (randomizer) {

        // retrieve the constrain function
        var constrain = randomizer.constrain;

        // Stub metadata
        var theDivisions = ["Finance", "Personnel", "Admin", "Facilities", "Transport", "Mail", "Info Services"];

        var theCustomers = ["ACME Ltd", "Bower Ltd", "Calvin and Company", "Dell Inc", "EveryNote.com"];

        var theCategories = ["purchasing", "urgent", "data-protection", "safety", "optional", "party"];

        var thePriorities = ['High', 'Medium', 'Low'];

        var theStates = ["scoped", "pending", "live", "suspended", "complete"];

        var theReportTypes = [ "Comment", "Report", "Audit"];

        var theUsers = [];

        var divLen = theDivisions.length;

        for (var i = 0; i < 25; i++) {
            var theDiv = theDivisions[constrain(0, divLen-1)];
            var isHod = i >= 20;
            var isCos = i >= 25;
            var uName;
            if (isCos) {
                uName = "Mgr_";
            } else if (isHod) {
                uName = "Hod_";
            } else {
                uName = "User_";
            }

            theUsers[i] = {
                "id": i + 1,
                "name": uName + (i + 1),
                "division": theDiv.name,
                "isHod": isHod,
                "isCos": isCos};
        }

        // Public API here
        return {
            /**
             * @method getUsers
             * @returns {Array} a list of user objects (id, name, plus others)
             */
            getUsers: function () {
                return theUsers;
            },

            /**
             * @method getUser
             * @returns {Array} the logged in user
             */
            getCurrentUser: function () {
                return theUsers[0];
            },

            /**
             * @method getDivisions
             * @returns {Array} a list of division objects (id plus name)
             */
            getDivisions: function () {
                return theDivisions;
            },

            /**
             * @method getCustomers
             * @returns {Array} a list of customer organisation objects (id plus name)
             */
            getCustomers: function () {
                return theCustomers;
            },

            /**
             * @method getCategories
             * @returns {Array} a list of user categories (id plus name)
             */
            getCategories: function () {
                return theCategories;
            },

            /**
             * @method getStates
             * @returns {Array} a list of acceptable states
             */
            getStates: function () {
                return theStates;
            },

            /**
             * @method getReportTypes
             * @returns {Array} a list of types of report submitted
             */
            getReportTypes: function () {
                return theReportTypes;
            },

            /**
             * @method getPriorities
             * @returns {Array} a list of acceptable priorities
             */
            getPriorities: function () {
                return thePriorities;
            }
        };

});
