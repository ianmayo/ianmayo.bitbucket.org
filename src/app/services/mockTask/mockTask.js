angular.module('mockTask', [
    'metadata',
    'randomizer'
])

.service('mockTask', function (randomizer, metadata) {
        // convenience var to ease access to randomizer funcs
        var constrain = randomizer.constrain;
        var randomBool = randomizer.randomBoolean;

        // support for text generation

        // Begin generator
        var fixie_wordlibrary = [
            "I", "8-bit", "ethical", "reprehenderit", "delectus", "non", "latte", "fixie", "mollit",
            "authentic", "1982", "moon", "helvetica", "dreamcatcher", "esse", "vinyl", "nulla", "Carles",
            "bushwick", "bronson", "clothesline", "fin", "frado", "jug", "kale", "organic", "local",
            "fresh", "tassel", "liberal", "art", "the", "of", "bennie", "chowder", "daisy", "gluten", "hog",
            "capitalism", "is", "vegan", "ut", "farm-to-table", "etsy", "incididunt", "sunt", "twee",
            "yr", "before", "gentrify", "whatever", "wes", "Anderson", "chillwave", "dubstep", "sriracha",
            "voluptate", "pour-over", "esse", "trust-fund", "Pinterest", "Instagram", "DSLR", "vintage",
            "dumpster", "totally", "selvage", "gluten-free", "brooklyn", "placeat", "delectus", "sint", "magna",
            "brony", "pony", "party", "beer", "shot", "narwhal", "salvia", "letterpress", "art", "party",
            "street-art", "seitan", "anime", "wayfarers", "non-ethical", "viral", "iphone", "anim", "polaroid",
            "gastropub", "city", 'classy', 'original', 'brew'
        ];

        function fixie_capitalize(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }

        function fixie_fetchWord() {
            return fixie_wordlibrary[constrain(0, fixie_wordlibrary.length - 1)];
        }

        function fixie_fetch(min, max, func, join) {
            var fixie_length;
            var result = [];

            if (!join) {
                join = ' ';
            }

            fixie_length = constrain(min, max);
            for (var fixie_i = 0; fixie_i < fixie_length; fixie_i++) {
                result.push(func());
            }

            return fixie_capitalize(result.join(join));
        }

        function fixie_fetchSentence() {
            return fixie_fetch(5, 8, fixie_fetchWord) + '.';
        }

        function fixie_fetchShortSentence() {
            return fixie_fetch(2, 5, fixie_fetchWord) + '.';
        }

        function fixie_fetchParagraph() {
            return fixie_fetch(3, 7, fixie_fetchSentence);
        }

        function fetch_suroundWithTag(min, max, func, tagName) {
            var startTag = '<' + tagName + '>';
            var endTag = '</' + tagName + '>';

            return startTag + fixie_fetch(min, max, func, endTag + startTag) + endTag;
        }

        function fixie_fetchParagraphs(min, max) {
            return fetch_suroundWithTag(min, max, fixie_fetchParagraph, 'p');
        }

        // some support functions to help with creating a task
        var theStates = metadata.getStates();

        var getStatus = function () {
            return theStates[constrain(0, theStates.length-1)];
        };

        var getPriority = function () {
            var thePriorities = metadata.getPriorities();
            return thePriorities[constrain(0, thePriorities.length-1)];
        };

        var getUser = function () {
            var theList = metadata.getUsers();
            return theList[constrain(0, theList.length-1)];
        };

        var getCustomer = function () {
            var theList = metadata.getCustomers();
            return theList[constrain(0, theList.length-1)];
        };

        var getCategories = function () {
            var res = [];
            var theList = metadata.getCategories();
            var numCats = constrain(1, 6);

            for (var i=0; i < numCats; i++) {
                // get a random index
                var thisIndex = constrain(1, theList.length) -1;

                // what's this category?
                var thisCat = theList[thisIndex];

                // do we have it already?
                if (_.indexOf(res, thisCat) < 0) {
                    res.push(thisCat);
                }
            }

            return res;
        };

        var getPeople = function () {
            var len = constrain(1,3);
            var people = [];

            for (var i=0; i<len; i++) {
                people[i] = getUser();
            }

            return people;
        };

        var getReport = function () {
            return fixie_fetchSentence();
        };

        var getDivision = function () {
            var theDivs = metadata.getDivisions();
            return theDivs[constrain(1, theDivs.length)-1];
        };

        var getDateAfter = function (maxDays, origin) {
            // create an origin, if we have to
            var theOrigin = origin;
            // reemmber the length of a day
            var aDay = 1000 * 60 * 60 * 24;

            if (!theOrigin) {
                theOrigin = new Date(2013, 7, 1);
            }

            // add a random number of days
            return new Date(theOrigin.getTime() + aDay * constrain(1, maxDays));
        };

        var getDatePeriod = function () {
            var startDate = getDateAfter(270);
            var endDate = getDateAfter(200, startDate);

            return {"start": startDate, "end": endDate};
        };

        var getItems = function () {
            var items = [];
            var len = constrain(1, 5);
            var theseDates;

            for (var i = 0; i < len; i++) {
                theseDates = getDatePeriod();

                items[i] = {
                    name: fixie_fetchShortSentence(),
                    start: theseDates.start,
                    finish: theseDates.end,
                    percent: 34,
                    people: getPeople(3),
                    description: fixie_fetchShortSentence(),
                    isDeliverable: randomBool(),
                    isTrial: randomBool(),
                    isPublication: randomBool(),
                    isFinancial: randomBool()
                };
            }

            return items;
        };

        var getActivities = function (taskId) {
            var items = [];
            var len = constrain(2, 15);
            var theReportTypes = metadata.getReportTypes();
            var lastDate = getDateAfter(200);
            var thisType;

            for (var i = len - 1; i >= 0; i--) {
                // sort out the report type
                thisType = theReportTypes[constrain(0, 2)];

                // sort out a date that occurs after the previous one
                lastDate = getDateAfter(45, lastDate);

                // create the new item
                items[i] = {
                    report: fixie_fetchShortSentence(),
                    author: getUser(),
                    date: lastDate,
                    type: thisType,
                    taskId: taskId
                };

                // was it a status report
                if (thisType == "Report") {
                    // and store a status for the report
                    items[i].status = getStatus();
                }
            }

            return items;
        };

        // and now generate the tasks themselves
        var theTasks = [];

        for (var i = 0; i < 20; i++) {
            theTasks[i] = {
                id: i,
                title: fixie_fetchSentence(),
                leader: getUser().name,
                status: getStatus(),
                latestReport: getReport(),
                division: getDivision(),
                categories: getCategories(),
                customer_name: "The Customer",
                customer_tally: "VP Purchasing",
                customer_phone: "01333 323232",
                customer_organisation: getCustomer(),
                description: fixie_fetchParagraphs(2, 4),
                background: fixie_fetchParagraphs(2, 4),
                impact: fixie_fetchParagraphs(1, 3),
                activities: getActivities(i),
                priority: getPriority(),
                resources: fixie_fetchParagraphs(1, 2),
                items: getItems()
            };
        }

        // Public API here
        return {
            /**
             * Retrieve the full list of tasks
             * @method getTasks
             * @returns {Array} the list
             */
            getTasks: function () {
                return theTasks;
            },

            /**
             * Add a new task
             * @method add
             * @param {Object} newTask an object representing the new task
             * @returns {Number} the new id
             */
            add: function (newTask) {
                // sort out the length, which we will laziily use as new id
                var len = theTasks.length;

                // create the task
                newTask.id = len;

                // ok, now store it
                theTasks[len] = newTask;

                return len;
            },

            /**
             * Change an existed task
             * @method change
             * @param {int} taskId task to change
             * @param {string} fieldName task property name
             * @param {number|date|string|Array} newValue new value
             */
            change: function (taskId, fieldName, newValue) {
                var requiredProperties = ['title', 'leader', 'division'];

                if (_.contains(requiredProperties, fieldName) && !newValue) {
                    // TODO: this is a required field, but no value has been assigned
                    //     - should we fire some error back to the calling code?
                    //     I'm going to comment out the return line since WebStorm
                    //     is reporting "Function with inconsistent returns"
                    // return " ";
                } else {
                    theTasks[taskId][fieldName] = newValue;
                }
            },

            /**
             * Add a comment to a task
             * @method addComment
             * @param {Object} comment an object representing a new comment
             */
            addComment: function(comment) {
                // sort out the length, which we will laziily use as a new activity
                var len = theTasks[comment.taskId].activities.length;

                theTasks[comment.taskId].activities[len] = comment;
            }
        };
    });
