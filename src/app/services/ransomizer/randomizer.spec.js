describe('randomizer', function () {
    beforeEach(module('randomizer'));

    // NOTE: this is how you retrieve a data object (service) that can be made available inside the tests
    beforeEach(inject(function ($injector) {
        randomizer = $injector.get('randomizer');
    }));

    var minV = 1;
    var maxV = 10;

    it('should be less/equal to max', inject(function () {
        var res = randomizer.constrain(minV, maxV);
        expect(res <= maxV).toBeTruthy();
    }));

    it('should be greater/equal to min', inject(function () {
        var res = randomizer.constrain(minV, maxV);
        expect(res >= minV).toBeTruthy();
    }));

    // TODO: to gain further confidence in the constrain function, it should be called more times, but Lint would not let
    // TODO:  = me put the test(s) into a for loop.

});
