angular.module('randomizer', [])

.service('randomizer', function () {

        // random number support, taken from: http://stackoverflow.com/a/424381/92441

        function nextRandomNumber() {
            var hi = this.seed / this.Q;
            var lo = this.seed % this.Q;
            var test = this.A * lo - this.R * hi;
            if (test > 0) {
                this.seed = test;
            } else {
                this.seed = test + this.M;
            }
            return (this.seed * this.oneOverM);
        }

        function RandomNumberGenerator(Seed) {
            this.seed = Seed;
            this.A = 48271;
            this.M = 2147483647;
            this.Q = this.M / this.A;
            this.R = this.M % this.A;
            this.oneOverM = 1.0 / this.M;
            this.next = nextRandomNumber;
            return this;
        }

        function createRandomNumber(Min, Max) {
            return Math.round((Max - Min) * rand.next() + Min);
        }

        //  seed the random number generator
        var rand = new RandomNumberGenerator(12);

        // Public API here
        return {
            /**
             * get random index
             * @param min the returned number will be equal to, or above this
             * @param max the returned number will be equal to, or below this
             * @returns a random integer
             */
            constrain: function (min,max) {
                return createRandomNumber(min, max);
            },
            /**
             * get a random boolean value
             * @returns boolean value that may be true or false
             */
            randomBoolean: function() {
                return Math.random() >= 0.5;
            }
        };

});
