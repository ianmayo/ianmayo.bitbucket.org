/**
 * @module filterParser
 */

angular.module('filterParser', [])

/**
 * @module filterParser
 * @class Service
 * @description This service provides 2 convenient methods first parses "filters"
 * query string and return "filters" object, second does the same thing only in reverse.
 */

.service('filterParser', function () {

    return {

        /**
         * It returns "filters" object based on the "filters" query string
         * @param path
         * @returns {{}}
         */
        parseString: function (path) {
            var query = {};

            if (!_.isString(path)) {
                return query;
            }

            var items = path.split('/');

            angular.forEach(items, function (value) {
                var item = value.split(':');

                if (item[0] && item[1]) {
                    query[item[0]] = item[1].split('+');
                }
            });

            return query;
        },

        /**
         * It returns "filters" query string based on the "filters" object
         * @param filters
         * @returns {string}
         */
        buildString: function (filters) {
            var query = '';

            if (!_.isObject(filters)) {
                return query;
            }

            angular.forEach(filters, function (value, key) {
                if (value.length) {
                    query += key + ':' + value.join('+') + '/';
                }
            });

            return query.replace(/\/$/, '');
        }
    };
});
