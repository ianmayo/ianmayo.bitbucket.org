describe('filterParser', function () {

    var filterParser;

    var stubFilterParam = 'status:scoped+pending+live/division:20/leader:12';

    var stubQuery = {
        status: ['scoped', 'pending', 'live'],
        division: ['20'],
        leader: ['12']
    };

    beforeEach(module('filterParser'));

    beforeEach(inject(function ($injector) {
        filterParser = $injector.get('filterParser');
    }));

    describe('parseString', function () {

        it('should return empty query object if there was not specified the correct string', inject(function () {
            expect(filterParser.parseString()).toEqual({});
            expect(filterParser.parseString('')).toEqual({});
            expect(filterParser.parseString({})).toEqual({});
            expect(filterParser.parseString([])).toEqual({});
        }));

        it('should get correctly parsed "statuses", "leader", "division" filters', inject(function () {
            expect(filterParser.parseString(stubFilterParam)).toEqual(stubQuery);
        }));

        it('should return correct data if "leader" filter was set correctly', inject(function () {
            expect(filterParser.parseString('status:complete+live+pending/leader:12/division:20/').leader)
                .toEqual(stubQuery.leader);
            expect(filterParser.parseString('status:/leader:12/division:20/').leader).toEqual(stubQuery.leader);
            expect(filterParser.parseString('status:/leader:12/division:').leader).toEqual(stubQuery.leader);
            expect(filterParser.parseString('status:/leader:12/').leader).toEqual(stubQuery.leader);
            expect(filterParser.parseString('status:/leader:12').leader).toEqual(stubQuery.leader);
            expect(filterParser.parseString('leader:12').leader).toEqual(stubQuery.leader);
            expect(filterParser.parseString('leader:12/').leader).toEqual(stubQuery.leader);
            expect(filterParser.parseString('/leader:12/').leader).toEqual(stubQuery.leader);
            expect(filterParser.parseString('/leader:12/leader:12/').leader).toEqual(stubQuery.leader);
        }));

        it('should return "null" if "leader" filter was not set correctly', inject(function () {
            expect(filterParser.parseString('status:complete+live+pending/leader:/division:20/').leader)
                .toBeUndefined();
            expect(filterParser.parseString('status:complete+live+pending/leader/division:20/').leader)
                .toBeUndefined();
            expect(filterParser.parseString('status:complete+live+pending//division:20/').leader).toBeUndefined();
            expect(filterParser.parseString('leader:/division:20/').leader).toBeUndefined();
            expect(filterParser.parseString('/leader:').leader).toBeUndefined();
            expect(filterParser.parseString('/leader:/leader:').leader).toBeUndefined();
        }));

        it('should return correct data if "division" filter was set correctly', inject(function () {
            expect(filterParser.parseString('status:complete+live+pending/leader:12/division:20/').division)
                .toEqual(stubQuery.division);
            expect(filterParser.parseString('+live+pending/leader:12/division:20/').division)
                .toEqual(stubQuery.division);
            expect(filterParser.parseString('ve+pending/leader:/division:20').division).toEqual(stubQuery.division);
            expect(filterParser.parseString('live+pending:/division:20').division).toEqual(stubQuery.division);
            expect(filterParser.parseString('+l/division:20').division).toEqual(stubQuery.division);
            expect(filterParser.parseString('division:20').division).toEqual(stubQuery.division);
            expect(filterParser.parseString('division:20/division:20').division).toEqual(stubQuery.division);
        }));

        it('should return "null" if "division" filter was not set correctly', inject(function () {
            expect(filterParser.parseString('status:complete+live+pending/leader:12/division:/').division)
                .toBeUndefined();
            expect(filterParser.parseString('status:complete+live+pending//division:').division).toBeUndefined();
            expect(filterParser.parseString('pending//division:').division).toBeUndefined();
            expect(filterParser.parseString('/division:').division).toBeUndefined();
            expect(filterParser.parseString('division:').division).toBeUndefined();
        }));

        it('should return correct data if "status" filter was set correctly', inject(function () {
            expect(filterParser.parseString('status:scoped+pending+live/leader:12/division:20/').status)
                .toEqual(stubQuery.status);
            expect(filterParser.parseString('status:scoped+pending+live/division:20/').status).toEqual(stubQuery.status);
            expect(filterParser.parseString('status:scoped+pending+live').status).toEqual(stubQuery.status);
            expect(filterParser.parseString('/division:20/status:scoped+pending+live').status).toEqual(stubQuery.status);
            expect(filterParser.parseString('/division:20/status:scoped+pending+live/leader:12').status)
                .toEqual(stubQuery.status);
            expect(filterParser.parseString('/division:20/leader:12/status:scoped+pending+live').status)
                .toEqual(stubQuery.status);
            expect(filterParser.parseString('status:complete/division:20/leader:12/').status).toEqual(['complete']);
            expect(filterParser.parseString('status:complete+live/division:20/leader:12/').status)
                .toEqual(['complete', 'live']);
        }));

        it('should return "null" if "status" filter was not set correctly', inject(function () {
            expect(filterParser.parseString('status/leader:12/division:23/').status).toBeUndefined();
            expect(filterParser.parseString('status:/leader:12/division:23/').status).toBeUndefined();
            expect(filterParser.parseString('/leader:12/status:/division:23/').status).toBeUndefined();
            expect(filterParser.parseString('/leader:12/statuses:complete/division:23/').status).toBeUndefined();
            expect(filterParser.parseString('status:').status).toBeUndefined();
            expect(filterParser.parseString('____status:complete+live').status).toBeUndefined();
        }));
    });

    describe('buildString', function () {

        it('should return correct filters string', inject(function () {
            expect(filterParser.buildString(stubQuery)).toEqual(stubFilterParam);
        }));

        it('should return empty string if param was not specified or param does not have correct format',
            inject(function () {
            expect(filterParser.buildString()).toEqual('');
            expect(filterParser.buildString({})).toEqual('');
            expect(filterParser.buildString([])).toEqual('');
            expect(filterParser.buildString({id: {}})).toEqual('');
            expect(filterParser.buildString({status: { complete: true }})).toEqual('');
        }));

        it('should return correct string with "custom" parameter', inject(function () {
            expect(filterParser.buildString({ custom: ['custom'] })).toEqual('custom:custom');
        }));

        it('should return correct string with correct "id" and "custom" parameters', inject(function () {
            expect(filterParser.buildString(angular.extend(stubQuery, {
                id: ['0', '1', '2'],
                custom: ['custom']
            }))).toEqual(stubFilterParam + '/id:0+1+2/custom:custom');
        }));

        it('should return correct string with correct parameters', inject(function () {
            expect(filterParser.buildString({
                id: ['34', '51', '17'],
                division: ['My_Division']
            })).toEqual('id:34+51+17/division:My_Division');

            expect(filterParser.buildString({
                status: ['complete', 'live'],
                user: ['admin']
            })).toEqual('status:complete+live/user:admin');

            expect(filterParser.buildString({
                status: ['complete', 'live', 'suspended', 'pending'],
                user: ['John Smith']
            })).toEqual('status:complete+live+suspended+pending/user:John Smith');

            expect(filterParser.buildString({
                status: ['complete', 'live', 'suspended', 'pending'],
                user: ['John Smith'],
                division: ['Coca Cola + Pepsi']
            })).toEqual('status:complete+live+suspended+pending/user:John Smith/division:Coca Cola + Pepsi');

            expect(filterParser.buildString({
                status: ['complete', 'live', 'suspended', 'pending', 'scoped'],
                user: ['Ian'],
                division: ['Task Tracker']
            })).toEqual('status:complete+live+suspended+pending+scoped/user:Ian/division:Task Tracker');
        }));
    });
});
