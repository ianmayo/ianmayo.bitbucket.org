/**
 * @module restTask
 */

angular.module('tasks', ['ngResource', 'mockTask'])

.service('tasks', function restTasksService(restTasksHttp, mockTask, $q) {

    var statusNotAllowed = 404;

    return {

        /**
         * Get all tasks
         * @returns {promise}
         */
        getTasks: function () {
            var deferred = $q.defer();

            restTasksHttp.getAllTasks({username: 'admin'}, function (response) {
                deferred.resolve(response.GetAllTasksResult);
            });

            return deferred.promise;
        },

        /**
         * Get particular task by its Id
         * @param taskId
         * @returns {promise}
         */
        getTaskById: function (taskId) {
            var deferred = $q.defer();

            restTasksHttp.getTaskById({username: 'admin', id: taskId}, function (response) {
                deferred.resolve(response.GetTaskByIdResult);
            });

            return deferred.promise;
        },

        /**
         * Add new task
         * @param newTask
         * @returns {promise}
         */
        add: function (newTask) {
            var deferred = $q.defer();

            restTasksHttp.addTask({
                username: 'admin',
                task: newTask
            }, function success (response) {
                mockTask.add(newTask);
                deferred.resolve(response.AddTaskResult);
            }, function error (response) {
                if (response.status === statusNotAllowed) {
                    deferred.resolve();
                }
            });

            return deferred.promise;
        },

        /**
         * Change particular task by its Id
         * @param taskId
         * @param fieldName
         * @param newValue
         * @returns {promise}
         */
        change: function (taskId, fieldName, newValue) {
            var deferred = $q.defer();

            restTasksHttp.updateTask({
                username: 'admin',
                id: taskId,
                property: fieldName,
                value: newValue
            }, function success (response) {
                deferred.resolve(response.UpdateTasksResult);
            }, function error (response) {
                if (response.status === statusNotAllowed) {
                    deferred.resolve();
                }
            });

            return deferred.promise;
        },

        /**
         * Add new comment
         * @param comment
         * @returns {promise}
         */
        addComment: function (comment) {
            var deferred = $q.defer();

            restTasksHttp.addTaskReport({
                username: 'admin',
                id: comment.taskId,
                property: 'report',
                taskReport: comment
            }, function (response) {
                mockTask.addComment(comment);
                deferred.resolve(response.AddTaskReportResult);
            }, function error (response) {
                if (response.status === statusNotAllowed) {
                    deferred.resolve();
                }
            });

            return deferred.promise;
        }
    };
})

.factory('restTasksHttp', ['$resource', '$location', function ($resource, $location) {
    var origin = $location.protocol() +'://'+ $location.host() + ':' + $location.port();

    return $resource(origin + '/backend/TaskTrackService.svc/v1/:method/task:__ext1__/:id:__ext2__/:property/:value',
        {
            username: '@username'
        },
        {
            getAllTasks: {
                method: 'GET',
                params: {
                    method: 'get',
                    __ext1__: '.json'
                }
            },

            getTaskById: {
                method: 'GET',
                params: {
                    method: 'get',
                    id: '@id',
                    __ext2__: '.json'
                }
            },

            addTask: {
                method: 'POST',
                params: {
                    method: 'post'
                }
            },

            updateTask: {
                method: 'POST',
                params: {
                    method: 'put',
                    id: '@id',
                    property: '@property',
                    value: '@value'
                }
            },

            addTaskReport: {
                method: 'POST',
                params: {
                    method: 'post',
                    id: '@id',
                    property: '@property'
                }
            }
        }
    );
}]);


