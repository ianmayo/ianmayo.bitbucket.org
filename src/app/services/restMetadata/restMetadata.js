/**
 * @module restTask
 */

angular.module('restMetadata', ['ngResource'])

.service('restMetadata', ['metadataItems', '$q', function restMetadataService(metadataItems, $q) {
    var theReportTypes = [ "comment", "status", "audit"];
    var thePriorities = [
        {
            id: 1,
            value: 'High'
        },
        {
            id: 2,
            value: 'Medium'
        },
        {
            id: 3,
            value: 'Low'
        }
    ];

    return $q.all([
        metadataItems.itemType('u'),
        metadataItems.itemType('d'),
        metadataItems.itemType('o'),
        metadataItems.itemType('c'),
        metadataItems.itemType('s')
    ])
    .then(function (values) {
        return {
            /**
             * @method getUsers
             * @returns {Array} a list of user objects
             */
            getUsers: function () {
                return values[0];
            },

            /**
             * @method getUser
             * @returns {Array} the logged in user
             */
            getCurrentUser: function () {
                if (values[0]) {
                    return values[0][0];
                }
            },

            /**
             * @method getDivisions
             * @returns {Array} a list of division objects
             */
            getDivisions: function () {
                return values[1];
            },

            /**
             * @method getCustomers
             * @returns {Array} a list of customer organisation objects
             */
            getCustomers: function () {
                return values[2];
            },

            /**
             * @method getCategories
             * @returns {Array} a list of user categories
             */
            getCategories: function () {
                return values[3];
            },

            /**
             * @method getStates
             * @returns {Array} a list of acceptable states
             */
            getStates: function () {
                return values[4];
            },

            /**
             * @method getReportTypes
             * @returns {Array} a list of types of report submitted
             */
            getReportTypes: function () {
                return theReportTypes;
            },

            /**
             * @method getPriorities
             * @returns {Array} a list of acceptable priorities
             */
            getPriorities: function () {
                return thePriorities;
            }
        };
    });
}])

.factory('metadataItems', ['restMetadataHttp', '$q', function restMetadataItems(restMetadataHttp, $q) {
    var deferred = $q.defer();

    var getMetadata =  function () {
        restMetadataHttp.getMetadata({username: 'admin'}, function (response) {
            deferred.resolve(response.GetMetadataResult.metadata);
        });

        return deferred.promise;
    };

    return {
        itemType: function (typeAbbr) {
            var itemTypeDeferred = $q.defer();

            getMetadata().then(function (response) {
                var collectionType = _.find(response, function (collection) {
                    return typeAbbr === collection.type;
                });

                itemTypeDeferred.resolve(collectionType.items);
            });

            return itemTypeDeferred.promise;
        }
    };
}])

.factory('restMetadataHttp', ['$resource', '$location', function restMetadataHttp($resource, $location) {
    var origin = $location.protocol() +'://'+ $location.host() + ':' + $location.port();

    return $resource(origin + '/backend/TaskTrackService.svc/v1/:method/metadata:__ext1__/:listType/:id/:value/',
        {
            username: '@username'
        },
        {
            getMetadata: {
                method: 'GET',
                params: {
                    method: 'get',
                    __ext1__: '.json'
                }
            },

            updateMetadataItem: {
                method: 'POST',
                params: {
                    method: 'put',
                    listType: '@listType',
                    id: '@id',
                    value: '@value'
                }
            },

            deleteMetadataItem: {
                method: 'POST',
                params: {
                    method: 'delete',
                    listType: '@listType',
                    id: '@id'
                }
            }
        }
    );
}]);


