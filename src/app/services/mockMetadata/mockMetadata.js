angular.module('mockMetadata', [])

.service('mockMetadata', function (mockMetadataCollections) {
    var theReportTypes = [ "comment", "status", "audit"];
    var thePriorities = [
        {
            id: 1,
            value: 'High'
        },
        {
            id: 2,
            value: 'Medium'
        },
        {
            id: 3,
            value: 'Low'
        }
    ];

    function collectionType(typeAbbr) {
        var collection = _.find(mockMetadataCollections, function (collection) {
            return typeAbbr === collection.type;
        });

        return collection.items;
    }

    return {
        getUsers: function () {
            return collectionType('u');
        },

        getCurrentUser: function () {
            return collectionType('u')[0];
        },

        getDivisions: function () {
            return collectionType('d');
        },

        getCustomers: function () {
            return collectionType('o');
        },

        getCategories: function () {
            return collectionType('c');
        },

        getStates: function () {
            return collectionType('s');
        },

        getReportTypes: function () {
            return theReportTypes;
        },

        getPriorities: function () {
            return thePriorities;
        }
    };
})

.value('mockMetadataCollections', [
        {
            "items": [
                {
                    "division": "DIV_1",
                    "id": 1,
                    "isCOS": false,
                    "isCSA": false,
                    "isHoD": false,
                    "isOA": false,
                    "loginId": "aa",
                    "value": "PER_1"
                },
                {
                    "division": "DIV_1",
                    "id": 2,
                    "isCOS": false,
                    "isCSA": false,
                    "isHoD": false,
                    "isOA": false,
                    "loginId": "bb",
                    "value": "PER_2"
                },
                {
                    "division": "DIV_1",
                    "id": 4,
                    "isCOS": false,
                    "isCSA": false,
                    "isHoD": false,
                    "isOA": true,
                    "loginId": "dd",
                    "value": "PER_4"
                },
                {
                    "division": "DIV_1",
                    "id": 5,
                    "isCOS": false,
                    "isCSA": false,
                    "isHoD": false,
                    "isOA": false,
                    "loginId": "mayoi532",
                    "value": "PER_5"
                },
                {
                    "division": "DIV_1",
                    "id": 6,
                    "isCOS": false,
                    "isCSA": false,
                    "isHoD": true,
                    "isOA": true,
                    "loginId": "ff",
                    "value": "PER_6"
                },
                {
                    "division": "DIV_1",
                    "id": 7,
                    "isCOS": false,
                    "isCSA": true,
                    "isHoD": true,
                    "isOA": false,
                    "loginId": "gg",
                    "value": "PER_7"
                },
                {
                    "division": "DIV_1",
                    "id": 8,
                    "isCOS": true,
                    "isCSA": false,
                    "isHoD": false,
                    "isOA": true,
                    "loginId": "hh",
                    "value": "PER_8"
                },
                {
                    "division": "DIV_1",
                    "id": 9,
                    "isCOS": false,
                    "isCSA": false,
                    "isHoD": false,
                    "isOA": false,
                    "loginId": "ii",
                    "value": "PER_9"
                },
                {
                    "division": "DIV_1",
                    "id": 10,
                    "isCOS": false,
                    "isCSA": false,
                    "isHoD": false,
                    "isOA": false,
                    "loginId": "jj",
                    "value": "PER_10"
                },
                {
                    "division": "DIV_1",
                    "id": 11,
                    "isCOS": true,
                    "isCSA": false,
                    "isHoD": false,
                    "isOA": false,
                    "loginId": "kk",
                    "value": "PER_11"
                },
                {
                    "division": "DIV_1",
                    "id": 12,
                    "isCOS": false,
                    "isCSA": false,
                    "isHoD": false,
                    "isOA": false,
                    "loginId": "ll",
                    "value": "PER_12"
                },
                {
                    "division": "DIV_1",
                    "id": 13,
                    "isCOS": false,
                    "isCSA": false,
                    "isHoD": true,
                    "isOA": false,
                    "loginId": "mm",
                    "value": "PER_13"
                },
                {
                    "division": "DIV_1",
                    "id": 14,
                    "isCOS": false,
                    "isCSA": false,
                    "isHoD": false,
                    "isOA": false,
                    "loginId": "nn",
                    "value": "PER_14"
                },
                {
                    "division": "DIV_1",
                    "id": 15,
                    "isCOS": false,
                    "isCSA": false,
                    "isHoD": false,
                    "isOA": false,
                    "loginId": "oo",
                    "value": "PER_15"
                },
                {
                    "division": "DIV_1",
                    "id": 16,
                    "isCOS": true,
                    "isCSA": true,
                    "isHoD": true,
                    "isOA": true,
                    "loginId": "Admin",
                    "value": "Admin"
                },
                {
                    "division": "DIV_1",
                    "id": 18,
                    "isCOS": true,
                    "isCSA": true,
                    "isHoD": true,
                    "isOA": true,
                    "loginId": "SUM-W8R2\\Administrator",
                    "value": "SUM-W8R2\\Administrator"
                }
            ],
            "type": "u"
        },
        {
            "items": [
                {
                    "id": 1,
                    "value": "DIV_1"
                },
                {
                    "id": 2,
                    "value": "DIV_2"
                },
                {
                    "id": 3,
                    "value": "DIV_3"
                },
                {
                    "id": 4,
                    "value": "DIV_4"
                },
                {
                    "id": 5,
                    "value": "DIV_5"
                },
                {
                    "id": 6,
                    "value": "DIV_6"
                }
            ],
            "type": "d"
        },
        {
            "items": [
                {
                    "id": 1,
                    "value": "Tag_1"
                },
                {
                    "id": 2,
                    "value": "Tag_2"
                },
                {
                    "id": 3,
                    "value": "Tag_3"
                },
                {
                    "id": 4,
                    "value": "Tag_4"
                },
                {
                    "id": 5,
                    "value": "Tag_5"
                },
                {
                    "id": 6,
                    "value": "Tag_6"
                },
                {
                    "id": 7,
                    "value": "Tag_7"
                },
                {
                    "id": 8,
                    "value": "Tag_8"
                },
                {
                    "id": 9,
                    "value": "Tag_9"
                }
            ],
            "type": "c"
        },
        {
            "items": [
                {
                    "id": 1,
                    "value": "ORG_1"
                },
                {
                    "id": 2,
                    "value": "ORG_2"
                },
                {
                    "id": 3,
                    "value": "ORG_3"
                },
                {
                    "id": 4,
                    "value": "ORG_4"
                },
                {
                    "id": 5,
                    "value": "ORG_5"
                },
                {
                    "id": 6,
                    "value": "ORG_6"
                },
                {
                    "id": 7,
                    "value": "ORG_7"
                }
            ],
            "type": "o"
        },
        {
            "items": [
                {
                    "id": 1,
                    "value": "Being Scoped"
                },
                {
                    "id": 2,
                    "value": "Pending Acceptance"
                },
                {
                    "id": 3,
                    "value": "Ongoing"
                },
                {
                    "id": 4,
                    "value": "Suspended"
                },
                {
                    "id": 5,
                    "value": "Complete"
                },
                {
                    "id": 6,
                    "value": "Archived"
                },
                {
                    "id": 7,
                    "value": "Rejected"
                }
            ],
            "type": "s"
        }
    ]
);
