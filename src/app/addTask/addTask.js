/**
 * @module AddTask
 */
angular.module('taskApp.addTask', [
    'ui.state',
    'ui.event',
    'ui.bootstrap',
    'placeholders',
    'tasks',
    'titleService',
    'guidanceDirective',
    'tooltipWatchDirective',
    'addWorkItemDirective',
    'removeWorkItemDirective',
    'workItemTypeDirective',
    'restMetadata'
])

/**
 * @module AddTask
 * @class Route
 */
.config(function config ($stateProvider) {

    $stateProvider.state('add', {
        url: '/add',
        views: {
            main: {
                controller: 'AddTaskCtrl',
                templateUrl: 'addTask/addTask.tpl.html'
            }
        }
    });
})

/**
 * @module AddTask
 * @class AddTaskCtrl (controller)
 */
.controller('AddTaskCtrl', function AddTaskController ($scope, titleService) {

    titleService.setTitle('Add Task');

    /**
     * Introductory guidance model
     * @type {{collapsed: boolean}}
     */
    $scope.introGuidance = {
        collapsed: false
    };
})

/**
 * @module AddTask
 * @class AddTaskFormCtrl (controller)
 */
.controller('AddTaskFormCtrl',
    function AddTaskFormController ($scope, $location, tasks, addWorkItemConfig, guidanceConfig, restMetadata) {

    /**
     *  Set default values for a new work item
     *  We set custom values because they could be overridden in the ViewTask module
     */
    angular.extend(addWorkItemConfig.defaultWorkItem, {
        name: '',
        description: ''
    });

    /**
     * Metadata for select boxes on the Add task form
     */
    restMetadata.then(function fillUpMetadata(metadataApi) {
        $scope.availablePriorities = metadataApi.getPriorities();
        $scope.availableDivisions = metadataApi.getDivisions();
        $scope.availableOrganisations = metadataApi.getCustomers();
        $scope.availableCategories = metadataApi.getCategories();
        $scope.availableLeaders = metadataApi.getUsers();
    });

    /**
     * Adding task model
     */
    $scope.defaultTask = {
        title: null,
        leader: null,
        division: null,
        customerName: null,
        customerTally: null,
        customerPhone: null,
        description: null,
        impact: null,
        updates: [],
        resources: null,
        priority: null,
        customerOrganisation: null,
        categories: null,
        status: 'live',
        workItems: [
            angular.extend({}, addWorkItemConfig.defaultWorkItem)
        ]
    };

    $scope.validationTooltip = {
        shown: false
    };

    guidanceConfig.addIcon = function (field, tooltip) {
        field.after(tooltip);
    };

    var validateAddTaskForm = function () {

        // When we try to create the task and we have unpopulated fields on the form
        // we need to set them as a "dirty" manually to help triggering correct validation mechanism
        $scope.addTaskForm.title.$dirty = true;
        $scope.addTaskForm.leader.$dirty = true;
        $scope.addTaskForm.division.$dirty = true;

        // Set to dirty each required field in the workItemsForm(s)
        angular.forEach($scope.workItemsForm.$error.required, function (formItem) {
            formItem.item.$dirty = true;
        });

        var isAddTaskFormValid = $scope.defaultTask.workItems.length && $scope.addTaskForm.$valid;

        // Show validation tooltip(s) when form is invalid
        $scope.validationTooltip.shown = !isAddTaskFormValid;

        return isAddTaskFormValid;
    };

    $scope.createTask = function () {

        if (!validateAddTaskForm()) {
            return;
        }

        // Create the task (minus the id)
        tasks.add($scope.defaultTask);

        // Navigate to home page
        $location.path('/');
    };
});
