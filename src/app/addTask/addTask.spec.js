/**
 * Add Task module's specs
 */

describe('Add Task section', function () {
    var $scope, tasks, mockHttp, metadataCollections;

    beforeEach(module('taskApp.addTask'));
    beforeEach(module('mockMetadata'));

    beforeEach(inject(function ($rootScope, $controller, $injector, $httpBackend, mockMetadataCollections) {

        metadataCollections = mockMetadataCollections;

        mockHttp = $httpBackend;

        $scope = $rootScope.$new();

        $scope.addTaskForm = {
            title: {
                $dirty: false
            },
            leader: {
                $dirty: false
            },
            division: {
                $dirty: false
            }
        };

        $scope.workItemsForm = {
            $error: {
                required: []
            }
        };

        tasks = $injector.get('tasks');

        $controller('AddTaskCtrl', {
            $scope: $scope
        });

        mockHttp.when('GET', /get\/metadata/).respond({GetMetadataResult: {metadata :metadataCollections}});

        $controller('AddTaskFormCtrl', {
            $scope: $scope
        });

        mockHttp.flush();
    }));

    describe('AddTaskController', function () {

        it('should have introGuidance model and collapsed property', function () {
            expect($scope.introGuidance.collapsed).toBeFalsy();
        });
    });

    describe('AddTaskFormController', function () {

        it('should have add task model', function () {
            expect($scope.defaultTask).toBeTruthy();
        });

        it('should have work items list in the task model', function () {
            expect(_.isArray($scope.defaultTask.workItems)).toBeTruthy();
        });

        it('should have validation tooltip model and shown property', function () {
            expect($scope.validationTooltip.shown).toBeFalsy();
        });

        it('should have models (collections) for select boxes', function () {
            expect(_.isArray($scope.availablePriorities)).toBeTruthy();
            expect(_.isArray($scope.availableDivisions)).toBeTruthy();
            expect(_.isArray($scope.availableOrganisations)).toBeTruthy();
            expect(_.isArray($scope.availableCategories)).toBeTruthy();
            expect(_.isArray($scope.availableLeaders)).toBeTruthy();
        });

        it('should create new task', inject(function (tasks) {
            $scope.addTaskForm.$valid = true;

            mockHttp.when('POST', /post\/task\?username=admin/)
                .respond({AddTaskResult: parseInt(Math.random() * 100, 10 )});

            tasks.add({id:1}).then(function (data) {
                expect(data).toEqual(jasmine.any(Number));
            });

            mockHttp.flush();
        }));
    });
});
