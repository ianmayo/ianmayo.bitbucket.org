/**
 * This is the e2e scenario of AddTak module
 *
 */

describe('AddTask module', function () {
    var taskUrl = '/add';
    var listUrl = '/list';
    var url = '/base/build/index.html#' + taskUrl;

    var tooltipSelector = '.tooltip-inner';
    var workItemsTableSelector = '.workItems';
    var createTaskSelector = 'input[type="submit"]:first';
    var removeWorkItemSelector = '.removeWorkItem';
    var firstWorkItemSelector = workItemsTableSelector +' tbody tr:nth-of-type(1)';
    var secondWorkItemSelector = workItemsTableSelector +' tbody tr:nth-of-type(2)';
    var thirdWorkItemSelector = workItemsTableSelector +' tbody tr:nth-of-type(3)';
    var addWorkItemBtnSelector = workItemsTableSelector + ' + .tfoot .addWorkItem';

    var stubTask = {
        title: 'Test',
        name: 'Test',
        tally: 'Test',
        phone: '007007007',
        description: 'Test',
        impact: 'Test',
        resource: 'Test',
        priority: 'High',
        leader: 'Hod_24',
        division: 'Admin',
        organisation: 'ACME Ltd',
        categories: ['urgent'],
        workItemName: 'Item'
    };

    beforeEach(function () {
        browser().navigateTo(url);
    });

    describe('Initial', function () {
        it('initial state', function () {
            expect(browser().window().hash()).toEqual(taskUrl);
        });
    });

    describe('AddWorkItem module', function () {
        var workItemsSelector = workItemsTableSelector + ' tbody tr.parent-line';

        it('should have work items element', function () {
            // work items table
            expect(element(workItemsTableSelector).count()).toEqual(1);
            // add work item button
            expect(element(addWorkItemBtnSelector).count()).toEqual(1);
        });

        it('should add new a work item to existed list', function () {
            // by default a new task should have 1 item
            expect(element(workItemsSelector).count()).toEqual(1);

            // click the New Work Item button
            element(addWorkItemBtnSelector).click();
            sleep(1);
            // should be added only one item
            expect(element(workItemsSelector).count()).toEqual(2);
        });
    });

    describe('Create Task module', function () {

        it('should show validation tooltips when we don\'t populate any fields', function () {
            var tooltip = element(tooltipSelector);

            element(createTaskSelector).click();
            expect(element(tooltipSelector).count()).toEqual(2);
        });

        it('should show validation tooltips when we populate each field without WorkItems', function () {
            input('defaultTask.title').enter(stubTask.title);
            select('defaultTask.leader').option(stubTask.leader);
            select('defaultTask.division').option(stubTask.division);
            element(createTaskSelector).click();

            expect(element(tooltipSelector).count()).toEqual(2);
        });

        it('should show validation tooltips when we populate only WorkItems without required fields', function () {
            var tooltip = element(tooltipSelector);

            using(firstWorkItemSelector).input('workItem.name').enter(stubTask.workItemName);
            element(createTaskSelector).click();

            expect(element(tooltipSelector).count()).toEqual(2);
        });


        it('should show validation tooltips when we populate each required field and delete all WorkItems',
            function () {

            input('defaultTask.title').enter(stubTask.title);
            select('defaultTask.leader').option(stubTask.leader);
            select('defaultTask.division').option(stubTask.division);

            using(firstWorkItemSelector).input('workItem.name').enter(stubTask.name);

            element(removeWorkItemSelector).click();
            element(createTaskSelector).click();

            expect(element(tooltipSelector).count()).toEqual(2);
        });


        it('should create the task with 3 work items', function () {
            input('defaultTask.title').enter(stubTask.title);
            select('defaultTask.leader').option(stubTask.leader);
            select('defaultTask.division').option(stubTask.division);
            element(addWorkItemBtnSelector).click();
            element(addWorkItemBtnSelector).click();

            using(firstWorkItemSelector).input('workItem.name').enter(stubTask.name);
            using(secondWorkItemSelector).input('workItem.name').enter(stubTask.name);
            using(thirdWorkItemSelector).input('workItem.name').enter(stubTask.name);
            element(createTaskSelector).click();

            expect(browser().window().hash()).toEqual(listUrl);
        });
    });

    /* an assortment of other things that I can think of testing:
     *
     * 1. Validation: user can press create task, but errors are displayed as follows:
     * 1a. Title must be present
     * 1b. Task Leader must be assigned
     * 1c. Division must be assigned
     * 1d. there must be a work item present.  The work item must have title, % complete (default 0), finish date.
     * Start date & assignments optional.
     *
     * 2. Close/reveal for top-page guidance
     * 3. Delete work item
     * 4. Add work item
     *
     * Yes, I'm sure there are lots more
     */

    describe('workItemType module', function () {
        var workItemSelector = workItemsTableSelector + ' tbody tr:first';
        var workItemTypeSelector = workItemSelector + ' .workItemType';
        var typeSelector = workItemTypeSelector + ' a';

        var testType = function (selector) {
            expect(element(selector).attr('class')).toContain('active');
            element(selector).click();
            sleep(1);
            expect(element(selector).attr('class')).not().toContain('active');
        };

        var setTypeActive = function (selector) {
            element(selector).query(function (type, done) {
                if (!type.hasClass('active')) {
                    type.click();
                    sleep(1);
                }
                testType(selector);

                done();
            });
        };

        beforeEach(function () {
            // click the New Work Item button
            element(addWorkItemBtnSelector).click();
        });

        it('should have work item element', function () {
            // check if work items table exists
            expect(element(workItemsTableSelector).count()).toEqual(1);
            // check if work item row exists
            expect(element(workItemSelector).count()).toEqual(1);
            // check if work item types directive exists
            expect(element(workItemTypeSelector).count()).toEqual(1);
            // check if work item types directive exists
            expect(element(typeSelector).count()).toEqual(4);
        });

        it('should change "Deliverable" type', function () {
            var selector = typeSelector + '[title="Deliverable"]';
            setTypeActive(selector);
        });

        it('should change "isTrial" type', function () {
            var selector = typeSelector + '[title="Trial"]';
            setTypeActive(selector);
        });

        it('should change "isPublication" type', function () {
            var selector = typeSelector + '[title="Publication"]';
            setTypeActive(selector);
        });

        it('should change "isFinancial" type', function () {
            var selector = typeSelector + '[title="Finance"]';
            setTypeActive(selector);
        });
    });
});
