/**
 * This is the e2e scenario of main module
 *
 * Notice for future we definitely should have several users with static data for the tests
 * Also we should notice that we should avoid real AJAX calls in e2e to make it as fast as it can be it means
 * that we have to switch on our future data services to the mock JSON data when we run e2e
 */

describe('Main module', function() {
    var url = '/base/build/index.html#/';
    var firstTaskSelector = '.tasks-list .task-item:first';
    var lastTaskSelector = '.tasks-list .task-item:last';
    var firstTaskContainer = firstTaskSelector + ' > .work-items-container';
    var lastTaskContainer = lastTaskSelector + ' > .work-items-container';

    var firstTimePeriodSelector = '.tasks-list .timePeriod:first';
    var lastTimePeriodSelector = '.tasks-list .timePeriod:last';
    var firstTimePeriodLabelSelector = firstTimePeriodSelector + ' .view-period';
    var lastTimePeriodLabelSelector = lastTimePeriodSelector + ' .view-period';
    var firstTimePeriodToggleSelector = firstTimePeriodSelector + ' .dropdown-toggle';
    var firstTimePeriodMenuSelector = firstTimePeriodSelector + ' .dropdown-menu';

    var firstTimeBarAxisSelector = '.tasks-list .timeBarAxis:first';
    var lastTimeBarAxisSelector = '.tasks-list .timeBarAxis:last';
    var firstTimeBarAxisColumnsSelector = firstTimeBarAxisSelector + ' > .axisColumn';
    var lastTimeBarAxisColumnsSelector = lastTimeBarAxisSelector + ' > .axisColumn';

    var firstWorkItemsTableSelector = '.tasks-list .workItems:first';
    var firstTimeBarSelector = firstWorkItemsTableSelector + ' .timeBar:first';
    var firstTimeBarCanvasSelector = firstWorkItemsTableSelector + ' .canvas:first';
    var firstTimeBarProgressWrapSelector = firstTimeBarSelector + ' > .startDateOffset';
    var firstTaskExpanderSelector = '.tasks-list .btn-expand:first';

    var addStatusReportBtnSelector =  '.btnStatusReport';
    var statusReportFormSelector =  '.formStatusReport';

    beforeEach(function () {
        browser().navigateTo(url);
    });

    describe('Initial state', function() {

        it('check if we are on the main page', function() {
            expect(browser().window().hash()).toEqual('/list');
        });
    });

    describe('Tasks list', function () {

        it('checks if ng-repeater works and we have list of tasks', function () {

            // check if tasks list has at least one element (task)
            expect(repeater('.tasks-list .task-item').count()).toBeGreaterThan(0);

            // set all of the status buttons to the "on" state
            element('.btnComplete, .btnSuspended').click();

            // check if all of the tasks are shown in the list
            expect(repeater('.tasks-list .task-item').count()).toEqual(20);
        });
    });

    describe('Status filters', function () {

        it('check if status filters work', function () {

            // switch off all of the filters
            element('.btnScoped, .btnPending, .btnLive').click();

            // check if there are no tasks
            expect(repeater('.tasks-list .task-item').count()).toEqual(0);

            // let's switch on complete status filter
            element('.btnComplete').click();

            // check if there are one complete task at least
            expect(repeater('.tasks-list .task-item').count()).toBeGreaterThan(0);

            // check if path has correct status filters
            expect(browser().window().hash()).toEqual('/list/status:complete');

            //switch on remaining filters
            element('.btnScoped, .btnPending, .btnLive, .btnSuspended').click();

            // check if there are all of the tasks
            expect(repeater('.tasks-list .task-item').count()).toEqual(20);

            // check if path has correct status filters
            expect(browser().window().hash()).toEqual('/list/status:complete+scoped+pending+live+suspended');

            // let's deactivate several of filters
            element('.btnLive, .btnSuspended').click();

            // we should have at least one task in the list
            expect(repeater('.tasks-list .task-item').count()).toBeGreaterThan(0);

            // check if path has correct status filters
            expect(browser().window().hash()).toEqual('/list/status:complete+scoped+pending');
        });

        it('check status:none filter', function () {

            // switch off all of the filters
            element('.btnScoped, .btnPending, .btnLive').click();

            // check if there is no tasks
            expect(repeater('.tasks-list .task-item').count()).toEqual(0);

            // check browser hash if there is status:none
            expect(browser().window().hash()).toEqual('/list/status:none');
        });
    });

    describe('Working items filters', function () {

        it('check if summary/details filters work', function () {

            // switch on details filter
            element('.details').click();

            // wait on animation a little bit
            sleep(1);

            /**
             * Why we check only first and last element here
             * So that's why e.g
             * expect(element('.tasks-list .task-item > .work-items-container').css('height')).not().toEqual('0px')
             * will take only first element for matching and we can't match them all in a loop as it could be expected
             */

                // check if the first task have expanded "in details" block
            expect(element(firstTaskContainer).css('height')).not().toEqual('0px');

            // check if the last task have expanded "in details" block
            expect(element(lastTaskContainer).css('height')).not().toEqual('0px');

            // switch on summary filter
            element('.summary').click();

            // wait on animation a little bit
            sleep(1);

            // check if the first task have collapsed "in details" block
            expect(element(firstTaskContainer).css('height')).toEqual('0px');

            // check if the last task have collapsed "in details" block
            expect(element(lastTaskContainer).css('height')).toEqual('0px');
        });

        it('check if single "working items" dropdown works', function () {

            // Let's create several convenient selectors
            var buttonsForClicking = firstTaskSelector + ' .btn-expand, ' + lastTaskSelector + ' .btn-expand';

            // click on the first and last "working items" dropdowns
            element(buttonsForClicking).click();

            // wait a bit
            sleep(1);

            // check if the first and last elements have none zero height
            expect(element(firstTaskContainer).css('height')).not().toEqual('0px');
            expect(element(lastTaskContainer).css('height')).not().toEqual('0px');

            // click on the first and last "working items" dropdowns
            element(buttonsForClicking).click();

            // wait a bit
            sleep(1);

            // check if the first and last elements have zero height
            expect(element(firstTaskContainer).css('height')).toEqual('0px');
            expect(element(lastTaskContainer).css('height')).toEqual('0px');
        });
    });

    describe('Tasks links', function () {

        it('should work', function () {

            // click on the first task's title
            element(firstTaskSelector + ' .task-title').click();

            // check if we are redirected to the correct URL
            // TODO uncomment it when mock task fixed
//            expect(browser().window().hash()).toEqual('/task/0');

            // let's return to the main page
            element('.brand').click();

            // click on the last task's title
            element(lastTaskSelector + ' .task-title').click();

            // check if we are on the correct URL
            // TODO uncomment it when mock task fixed
//            expect(browser().window().hash()).toEqual('/task/18');
        });
    });

    describe('Test status report window', function () {
        var initialStatus = 'scoped', changeStatus = 'complete';

        it('should have at least one status title on a page', function () {
            expect(element('.tasks-list .status').count()).toBeGreaterThan(0);
            // TODO uncomment it when mock task fixed
//            expect(element('.tasks-list .status:eq(0)').html()).toEqual(initialStatus);
            expect(element('.tasks-list .task-title').count()).toBeGreaterThan(0);
        });

        it('should have at least one button on page', function () {
            expect(element(addStatusReportBtnSelector).count()).toBeGreaterThan(0);
        });

        it('should have window directive', function () {
            // check element with directive
            expect(element(statusReportFormSelector).count()).toEqual(0);
        });

        it('window should have "Close" button', function () {
            // open modal window
            element(addStatusReportBtnSelector + ' :eq(0)').click();
            expect(element(statusReportFormSelector).count()).toEqual(1);

            // close window
            element(statusReportFormSelector +' .btnClose').click();
            expect(element(statusReportFormSelector).count()).toEqual(0);
        });

        it('window should have "Change" button', function () {
            var btnStatus = element('.tasks-list .status:eq(0)');

            // set filter 'complete'
            element('.filter .btnComplete').query(function (btn, done) {
                if (!btn.hasClass('active')) {
                    btn.click();
                }
                done();
            });

            expect(element('.tasks-list .status:eq(0)').count()).toEqual(1);
            // open modal window
            element(addStatusReportBtnSelector + ' :eq(0)').click();
            expect(btnStatus.count()).toEqual(1);

            // change status
            select('task.status').option(changeStatus);

            // change and close window
            element(statusReportFormSelector + ' .btnChange').click();
            expect(element(statusReportFormSelector).count()).toEqual(0);

            // check new status
            expect(btnStatus.html()).toEqual(changeStatus);
        });
    });

    describe('Focused field in the Add Status Report window', function () {
        it('should open a popup dialog and set a focus on the "comment" field', function () {
            // open modal window
            element(addStatusReportBtnSelector + ' :eq(0)').click();
            sleep(3);

            // check is element on focus
            expect(element(statusReportFormSelector + ' textarea:focus').count()).toBe(1);

            // close window
            element(statusReportFormSelector +' .btnClose').click();
            sleep(1);
        });
    });

    describe('Time period select', function () {
        it('should have at least one period select option', function () {
            expect(element(firstTimePeriodSelector).count()).toEqual(1);
            // open / close element
            expect(element(firstTimePeriodToggleSelector).count()).toEqual(1);
            // month list
            expect(element(firstTimePeriodMenuSelector).count()).toEqual(1);
        });

        it('should change months in all period selects', function () {
            var label;

            // select first option - 3 month period
            element(firstTimePeriodMenuSelector + ' > li:eq(0)').click();
            sleep(1);
            label = '3 Months';
            expect(element(firstTimePeriodLabelSelector).html()).toEqual(label);
            expect(element(lastTimePeriodLabelSelector).html()).toEqual(label);

            // select second option - 6 month period
            element(firstTimePeriodMenuSelector + ' > li:eq(1)').click();
            sleep(1);
            label = '6 Months';
            expect(element(firstTimePeriodLabelSelector).html()).toEqual(label);
            expect(element(lastTimePeriodLabelSelector).html()).toEqual(label);

            // select second option - 12 month period
            element(firstTimePeriodMenuSelector + ' > li:eq(2)').click();
            sleep(1);
            label = '12 Months';
            expect(element(firstTimePeriodLabelSelector).html()).toEqual(label);
            expect(element(lastTimePeriodLabelSelector).html()).toEqual(label);
        });
    });

    describe('Time axis bar', function () {
        var periodCheck = function(originDate) {
            // select 3 month period
            element(firstTimePeriodMenuSelector + ' > li:eq(0)').click();
            sleep(1);
            expect(element(firstTimeBarAxisColumnsSelector).count()).toEqual(3);
            expect(element(lastTimeBarAxisColumnsSelector).count()).toEqual(3);
            // a month label
            expect(element(firstTimeBarAxisColumnsSelector + ':first > .columnMonth').html()).toEqual(originDate);
            expect(element(lastTimeBarAxisColumnsSelector + ':first > .columnMonth').html()).toEqual(originDate);

            // select 6 month period
            element(firstTimePeriodMenuSelector + ' > li:eq(1)').click();
            sleep(1);
            expect(element(firstTimeBarAxisColumnsSelector).count()).toEqual(6);
            expect(element(lastTimeBarAxisColumnsSelector).count()).toEqual(6);
            // a month label
            expect(element(firstTimeBarAxisColumnsSelector + ':first > .columnMonth').html()).toEqual(originDate);
            expect(element(lastTimeBarAxisColumnsSelector + ':first > .columnMonth').html()).toEqual(originDate);

            // select 12 month period
            element(firstTimePeriodMenuSelector + ' > li:eq(2)').click();
            sleep(1);
            expect(element(firstTimeBarAxisColumnsSelector).count()).toEqual(12);
            expect(element(lastTimeBarAxisColumnsSelector).count()).toEqual(12);
            // a month label
            expect(element(firstTimeBarAxisColumnsSelector + ':first > .columnMonth').html()).toEqual(originDate);
            expect(element(lastTimeBarAxisColumnsSelector + ':first > .columnMonth').html()).toEqual(originDate);
        };

        it('should have at least one time axis bar', function () {
            expect(element(firstTimeBarAxisSelector).count()).toEqual(1);
            // axis columns
            expect(element(firstTimeBarAxisColumnsSelector).count()).toBeGreaterThan(1);
            // a month column
            expect(element(firstTimeBarAxisColumnsSelector + ':first > .columnMonth').count()).toEqual(1);
        });

        it('should change month columns on a page when time period changes', function () {
            var $filter = angular.injector(['ng']).get('$filter');
            var curMonth = $filter('date')(new Date(), 'MMM');

            periodCheck(curMonth);
        });

        it('should change month columns on a page when time period origin date changes', function () {
            var $filter = angular.injector(['ng']).get('$filter');
            var curDate = new Date();
            var prev3Month = $filter('date')(curDate.setMonth(curDate.getMonth() - 1), 'MMM');

            // click on the prev period button
            element(firstTimePeriodSelector + ' .btn:first').click();
            sleep(1);

            periodCheck(prev3Month);
        });
    });

    describe('Time bar', function () {
        var dateParseService = angular.injector(['dateParse']).get('dateParse');
        var timeBar;
        var testDirection;

        var timeBarVisibility = function(direction) {
            var itemFinishDate;
            var itemStartDate;
            var periodEndDate;
            var periodStartDate;

            var expandTask = function () {
                element(firstTaskExpanderSelector).query(function (btnExpand, expandDone) {
                    if (!btnExpand.hasClass('dropup')) {
                        btnExpand.click();
                    }
                    expandDone();
                });
            };

            var itemDates = function () {

                // define start date for time bars canvas
                periodStartDate = new Date();
                // a month starts from 1 day
                periodStartDate.setDate(1);

                // set period end date (3 month)
                periodEndDate = new Date(periodStartDate);
                periodEndDate.setMonth(periodEndDate.getMonth() + 3);

                element(firstWorkItemsTableSelector + ' tr:eq(1) > td:eq(3)')
                    .query(function (itemFinishDateCell, done) {
                        // start date
                        itemStartDate = dateParseService.fromISO(itemFinishDateCell.data('iso-date-start'));
                        // finish date
                        itemFinishDate = dateParseService.fromISO(itemFinishDateCell.data('iso-date-finish'));

                        done();
                    });
            };

            var moveToItemForward = function () {
                var changePeriodDirectionSel = firstTimePeriodSelector + ' .btn:last';

                element(changePeriodDirectionSel).query(function changePeriod(btn, done) {
                    // change period
                    btn.click();
                    sleep(0.5);

                    periodStartDate.setMonth(periodStartDate.getMonth() + 1);
                    periodEndDate.setMonth(periodEndDate.getMonth() + 1);

                    if (periodStartDate.getTime() < itemFinishDate.getTime() &&
                        itemFinishDate.getTime() < periodEndDate.getTime()) {
                        // right edge (item finish date) is visible
                        isItemVisible();
                        moveToItemForward();
                    } else if (periodStartDate.getTime() < itemStartDate.getTime() &&
                        periodEndDate.getTime() > itemStartDate.getTime()) {
                        // left edge (item start date) is visible
                        isItemVisible();
                    } else if (periodEndDate.getTime() < itemStartDate.getTime()) {
                        // finally, item is hidden
                        isItemInvisible();

                        // click to previous period and the item should be visible
                        element(firstTimePeriodSelector + ' .btn:first').click();
                        sleep(0.5);
                        isItemVisible();
                    } else if (itemFinishDate.getTime() < periodEndDate.getTime()) {
                        // an item is behind of canvas, there is no sense click forward
                        done();
                    } else {
                        // continue changing period, probably the item should be there
                        moveToItemForward();
                    }

                    done();
                });
            };

            var moveToItemBackward = function () {
                var changePeriodDirectionSel = firstTimePeriodSelector + ' .btn:first';

                element(changePeriodDirectionSel).query(function (btn, done) {
                    // change period
                    btn.click();
                    sleep(0.5);

                    periodStartDate.setMonth(periodStartDate.getMonth() - 1);
                    periodEndDate.setMonth(periodEndDate.getMonth() - 1);

                    if (periodStartDate.getTime() < itemFinishDate.getTime() &&
                        itemFinishDate.getTime() < periodEndDate.getTime()) {
                        // right edge (item finish date) is visible
                        isItemVisible();
                        moveToItemBackward();
                    } else if (periodStartDate.getTime() < itemStartDate.getTime() &&
                        periodEndDate.getTime() > itemStartDate.getTime()) {
                        // left edge (item start date) is visible
                        isItemVisible();
                        moveToItemBackward();
                    } else if (periodEndDate.getTime() < itemStartDate.getTime()) {
                        // finally, item is hidden
                        isItemInvisible();

                        // move to next period and the item should be visible
                        element(firstTimePeriodSelector + ' .btn:last').click();
                        sleep(0.5);
                        isItemVisible();

                    } else if (periodEndDate.getTime() < itemStartDate.getTime()) {
                        // an item is ahead of canvas , there is no sense click backward
                        done();
                    } else {
                        // continue changing period, probably the item should be there
                        moveToItemBackward();
                    }

                    done();
                });
            };

            var isItemVisible = function () {
                sleep(0.5);
                expect(element(firstTimeBarProgressWrapSelector).css('visibility')).toEqual('visible');
            };

            var isItemInvisible = function () {
                sleep(0.5);
                expect(element(firstTimeBarProgressWrapSelector).css('visibility')).toEqual('hidden');
            };

            expandTask();
            itemDates();

            if ('forward' === direction) {
                moveToItemForward();
            }

            if ('backward' === direction) {
                moveToItemBackward();
            }
        };

        it('should have at least one time bar element', function () {
            // work items table
            expect(element(firstWorkItemsTableSelector).count()).toEqual(1);

            // time bar directive element
            timeBar = element(firstTimeBarSelector);
            expect(timeBar.count()).toEqual(1);

            // canvas for time bar
            expect(element(firstTimeBarCanvasSelector).count()).toEqual(1);

            // container for progress bar
            expect(element(firstTimeBarProgressWrapSelector).count()).toEqual(1);

            // time bar percent completion
            expect(timeBar.attr('data-complete-percent')).toEqual('item.percent');
            // time bar item data
            expect(timeBar.attr('data-item')).toEqual('item');
            // time period start and time axis scale
            expect(timeBar.attr('data-time-axis')).toEqual('period');
            // task is collapsed
            expect(timeBar.attr('data-is-collapsed')).toEqual('task.isTaskCollapsed');
        });

        it('should test item visibility in the past', function () {
            var finishDateCell = element(firstWorkItemsTableSelector + ' tr:eq(1) > td:eq(3)');

            // select 3 month period
            element(firstTimePeriodMenuSelector + ' > li:eq(0)').click();
            sleep(1);

            expect(finishDateCell.count()).toEqual(1);
            expect(finishDateCell.attr('data-iso-date-start')).toBeDefined();
            expect(finishDateCell.attr('data-iso-date-finish')).toBeDefined();
            testDirection = 'backward';
            timeBarVisibility(testDirection);
        });

        it('should test item visibility in the future', function () {
            var finishDateCell = element(firstWorkItemsTableSelector + ' tr:eq(1) > td:eq(3)');

            // select 3 month period
            element(firstTimePeriodMenuSelector + ' > li:eq(0)').click();
            sleep(1);

            expect(finishDateCell.count()).toEqual(1);
            expect(finishDateCell.attr('data-iso-date-start')).toBeDefined();
            expect(finishDateCell.attr('data-iso-date-finish')).toBeDefined();
            testDirection = 'forward';
            timeBarVisibility(testDirection);
        });
    });
});
