/**
 * Main module's unit test spec
 */

describe('Main section', function() {
    beforeEach(module('taskApp.main'));
    beforeEach(module('mockTask'));

    describe('Controllers', function () {
        var $scope, rootScope, location, state, mockTasks;

        var mockTask = {
            id: 1,
            title: 'Task title',
            status: 'Being Scoped'
        };

        describe('MainCtrl', function () {

            beforeEach(inject(function ($rootScope, $controller, $location, $state, _mockTask_) {
                mockTasks = _mockTask_;

                rootScope = $rootScope;
                $scope = rootScope.$new();
                location = $location;
                state = $state;

                $controller('MainCtrl', {
                    $scope: $scope,
                    theseTasks: mockTasks.getTasks()
                });
            }));

            it('should have list of tasks', function () {
                expect($scope.theseTasks).toBeDefined();
                expect($scope.theseTasks.length).toBeGreaterThan(0);
            });

            it('should have "$scope.filters"', function () {
                expect($scope.filters).toBeDefined();
            });

            it('should have toggleFilter method', function () {
                expect($scope.toggleFilter).toBeDefined();
            });

            it('should have setSingleFilter method', function () {
                expect($scope.setSingleFilter).toBeDefined();
            });

            it('should have isFilter method', function () {
                expect($scope.isFilter).toBeDefined();
            });

            it('should remove "Pending Acceptance" status from the path and vice versa', function () {
                $scope.$emit('$stateChangeSuccess');
                $scope.toggleFilter('status', 'Pending Acceptance');
                expect(location.path()).toEqual('/list/status:Being Scoped+Ongoing/view:summary');

                state.params.filters = 'status:Being Scoped+Ongoing';
                $scope.$emit('$stateChangeSuccess');
                $scope.toggleFilter('status', 'Pending Acceptance');
                expect(location.path()).toEqual('/list/status:Being Scoped+Ongoing+Pending Acceptance/view:summary');
            });

            it('should add "Complete" status to the path and vice versa', function () {
                $scope.$emit('$stateChangeSuccess');
                $scope.toggleFilter('status', 'Complete');
                expect(location.path())
                    .toEqual('/list/status:Being Scoped+Pending Acceptance+Ongoing+Complete/view:summary');

                state.params.filters = 'status:Being Scoped+Pending Acceptance+Ongoing+Complete';
                $scope.$emit('$stateChangeSuccess');
                $scope.toggleFilter('status', 'Complete');
                expect(location.path()).toEqual('/list/status:Being Scoped+Pending Acceptance+Ongoing/view:summary');
            });

            it('should add "none" status to the path and vice versa', function () {
                state.params.filters = 'status:Pending Acceptance';
                $scope.$emit('$stateChangeSuccess');
                $scope.toggleFilter('status', 'Pending Acceptance');
                expect(location.path()).toEqual('/list/status:none/view:summary');

                state.params.filters = 'status:none';
                $scope.$emit('$stateChangeSuccess');
                $scope.toggleFilter('status', 'Pending Acceptance');
                expect(location.path()).toEqual('/list/status:Pending Acceptance/view:summary');
            });

            it('should add "deatil" and "lite" to the path', function () {
                state.params.filters = 'status:Pending Acceptance';
                $scope.$emit('$stateChangeSuccess');
                $scope.setSingleFilter('view', 'detail');
                expect(location.path()).toEqual('/list/status:Pending Acceptance/view:detail');

                state.params.filters = 'status:Ongoing';
                $scope.$emit('$stateChangeSuccess');
                $scope.setSingleFilter('view', 'lite');
                expect(location.path()).toEqual('/list/status:Ongoing/view:lite');
            });

            describe('TasksListCtrl', function () {

                beforeEach(inject(function ($controller) {
                    $scope = $scope.$new();

                    $controller('TasksListCtrl', {
                        $scope: $scope
                    });
                }));

                it('should have access to the parent controller\'s scope', function () {
                    expect($scope.theseTasks).toBeDefined();
                    expect($scope.filters).toBeDefined();
                });

                it('should have query function for the ng-repeater of the tasks list', function () {
                    expect(_.isFunction($scope.tasksQuery)).toBeTruthy();
                });

                it('should change local listView values if global one was changed', function () {
                    $scope.setSingleFilter('view', 'detail');
                    $scope.$digest();
                    $scope.setSingleFilter('view', 'lite');
                    $scope.$digest();

                    expect($scope.theseTasks).toBeDefined();
                    expect($scope.theseTasks.length).toBeGreaterThan(0);
                    expect($scope.theseTasks[0].detailView).toBeFalsy();
                });

                it('should process status report window', function () {
                    expect(_.isFunction($scope.openActivityWindow)).toBeTruthy();

                    spyOn($scope, '$broadcast');
                    $scope.openActivityWindow('report', mockTask);
                    expect($scope.$broadcast).toHaveBeenCalledWith('showActivityWindow',
                        {type: 'report', task: mockTask}
                    );
                });

                it('should have time period object', function () {
                    expect(_.isObject($scope.period)).toBeTruthy();
                    expect(_.isNumber($scope.period.months)).toBeTruthy();
                    // Current date object
                    expect(_.isObject($scope.period.start)).toBeTruthy();
                });
            });
        });
    });
});
