/**
 * @module Main
 * TODO: Make an agreement what length should have a file before splitting
 */

angular.module('taskApp.main', [
    'tasks',
    'titleService',
    'filterParser',
    'placeholders',
    'ui.state',
    'ui.bootstrap',
    'activityWindowDirective',
    'timePeriodDirective',
    'timeBarAxisDirective',
    'timeBarDirective'
])

/**
 * @module Main
 * @class Route
 */

.config(function config ($provide, $stateProvider) {

    /**
     * Special constant which is going to be used for "null" filters in the URL
     * for example "status:none, division:none" etc.
     */
    $provide.constant('noFilters', 'none');

    /**
     * Detail view constant.
     */
    $provide.constant('detailView', 'detail');

    /**
     * Filters by default, they populate $scope.filters object if there are no existing filters in the URL.
     * @type {Object}
     */
    $provide.value('defaultFilters', {
        status: [
            'Being Scoped',
            'Pending Acceptance',
            'Ongoing'
        ],
        view: [
            'summary'
        ]
    });

    /**
     * List of the keys which should be ignored during the tasks filtering.
     */
    $provide.value('taskQueryBlackKeys', ['view']);

    /**
     * Adapter to resolve situations when we have value "status:Ongoing" in the URL
     * and in data we have {status: {id:"1", value: "Ongoing"}} object for example.
     * It is used for the simple straightforward conversion in the "ng-repeat" filter query.
     */
    $provide.value('taskFilterAdapter', function (item, key) {
        return _.isObject(item[key]) ? item[key].value : item[key];
    });

    var defaultViews = {
        main: {
            controller: 'MainCtrl',
            templateUrl: 'main/main.tpl.html'
        }
    };

    $stateProvider.state('main', {
        url: '/list',
        views: defaultViews,
        resolve: {
            theseTasks: ['tasks', function (tasks) {
                return tasks.getTasks().then(function (tasks) {
                    return tasks;
                });
            }]
        }
    });

    $stateProvider.state('main.filters', {
        url: '/*filters',
        views: defaultViews
    });
})

/**
 * @module Main
 * @class MainCtrl (controller)
 */

.controller('MainCtrl', function MainController ($scope, $state, $location, titleService, filterParser, noFilters,
    theseTasks, defaultFilters) {

    titleService.setTitle('Home');

    /**
     * Filters object.
     * @type {Object}
     */
    $scope.filters = {};

    /**
     * Array of tasks also it's fake data for the time being.
     * @type {Array}
     */
    $scope.theseTasks = theseTasks;

    /**
     * Add filters to the URL path.
     */
    var addFiltersToPath = function () {
        $location.path('list/' + filterParser.buildString($scope.filters));
    };

    /**
     * Add default values to the $scope.filters.
     * @param filters
     */
    var setDefaultFilters = function (filters) {
        angular.forEach(filters, function (value, key) {
            if (!$scope.filters[key]) {
                $scope.filters[key] = angular.extend([], value);
            }
        });
    };

    /**
     * Add "none" value to the particular filter if this is empty and vice versa.
     * @param name
     * @param value
     */
    var toggleNoFilters = function (name, value) {
        $scope.filters[name] = _.without($scope.filters[name], value);

        if (!$scope.filters[name].length) {
            $scope.filters[name].push(value);
        }
    };

    /**
     * Get the particular filter and check if it has specified value.
     * In other words it is similar to Array.indexOf('').
     * @param name
     * @param value
     * @returns {Boolean}
     */
    $scope.isFilter = function (name, value) {
        return _.contains($scope.filters[name], value);
    };

    /**
     * Toggle value of the particular filter and add it to the URL path.
     * @param name
     * @param value
     */
    $scope.toggleFilter = function (name, value) {
        if (_.contains($scope.filters[name], value)) {
            $scope.filters[name] = _.without($scope.filters[name], value);
        } else {
            $scope.filters[name].push(value);
        }

        toggleNoFilters(name, noFilters);
        addFiltersToPath();
    };

    /**
     * Add single value to the particular filter and add it to the URL path.
     * @param name
     * @param value
     */
    $scope.setSingleFilter = function (name, value) {
        $scope.filters[name] = [value];
        addFiltersToPath();
    };

    /**
     * When $stateChangeSuccess is fired then it parses filters from the URL and add them to the $scope.filters object.
     */
    $scope.$on('$stateChangeSuccess', function () {
        $scope.filters = filterParser.parseString($state.params.filters);

        setDefaultFilters(defaultFilters);
    });
})

/**
 * @module Main
 * @class TasksListCtrl (controller)
 * @description This is the "child" controller of the MainCtrl which has access to the its $scope.
 */

.controller('TasksListCtrl', function TasksListCtrl ($scope, taskFilterAdapter, taskQueryBlackKeys, detailView) {

    /**
     * Show/hide tasks details when user change the global listView.detail state.
     */
    $scope.$watch('filters.view', function (value) {
        angular.forEach($scope.theseTasks, function (task) {
            task.detailView = value[0] === detailView;
        });
    });

    /**
     * This query is needed for the tasks list filtering. It's working based on the $scope.filters object.
     * @param task
     * @returns {Boolean}
     */
    $scope.tasksQuery = function (task) {
        var isValid = true;

        angular.forEach($scope.filters, function (value, key) {
            if (_.contains(taskQueryBlackKeys, key)) {
                return;
            }

            isValid = isValid && _.contains(value, taskFilterAdapter(task, key));
        });

        return isValid;
    };

    // TimePeriod directive
    $scope.period = {
        start: new Date(),
        months: 3
    };

    /**
     * Add report window
     * @param {String} type
     * @param {Object} task
     */
    $scope.openActivityWindow = function (type, task) {
        $scope.$broadcast('showActivityWindow', {type: type, task: task});
    };
});
